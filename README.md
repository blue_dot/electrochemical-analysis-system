一、引言
1.1 项目背景介绍
本项目《电化学分析系统》是一个新系统，本系统提供在线的电化学分析服务对硬件依赖性、可以提高检测设备的效率、促进检测设备的专门化发展、降低检测成本，系统将演化成诺干个版本最终实现专门化定制化的检测，并提供在线付费检测分析。

1.2编写目的
本文档编写目的是为开发人员明确系统设计和开发规范。具体说明了系统架构设计的目标和约束需求，规定了系统的开发环境和运行环境，阐述了逻辑架构设计的整体思想和具体设计的实现，介绍了系统的主要业务流程和系统的功能模块的划分以及模块之间的联系，表明了系统采用存储的数据库类型以及数据库又采用了什么设计编辑工具和数据库中存储的各种表的具体信息，最后是设计了系统核心功能的界面原型。

1.3 约束需求
本项目基于MVC模型开发，具体可分为边界层，控制层，应用层，数据操作层。
具体流程用活动图表示。

二、系统架构设计

图一 系统架构图

三、详细系统类设计
3.1 系统类图
《电化学分析系统》类图
图二 电化学分析系统整体类图

3.2 边界层
3.2.1 UserVO类说明
3.2.1.1 类说明表
类名称：	UserVO	所在层：	边界层		
主要功能：	主要负责对用户数据进行包装返回给前端界面展示，及接收前端输入数据				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | String | id  | 存放用户ID |  |
|  | private | String | userName | 代表用户名称 |  |
|  | private | String | userPassword | 存放用户密码 |  |
|  | private | String | userSex | 存放用户性别 |  |
|  | private | String | userPhone | 存放用户名称 |  |
|  | private | String | userAvatar | 存放用户头像 |  |
|  | private | String | userEmail | 存放用户邮箱 |  |
|  | private | Integer | userStatus | 表示用户状态 |  |
|  | private | List<Role> | userRole | 存放用户所有角色 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | String | 无 | getUserName | 获取用户名称 |
|  | public | String | 无 |  getUserSex | 获取用户性别 |
|  | public | String | 无 | getUserPhone | 获取用户电话号码 |
|  | public | String | 无 | getUserAvatar | 获取用户头像地址链接 |
|  | public | String | 无 | getUserEmail | 获取用户邮箱 |
|  | public | Integer | 无 | getUserStatus | 获取用户状态 |
|  | public | List<Role> | 无 | getUserRoles | 获取用户角色 |
| 涉及物理表： | 5.2.1用户表、5.2.3角色表 |  |  |  |  |

3.2.2 QueryVo类说明
3.2.2.1 类说明表
类名称：	QueryVo	所在层：	边界层		
主要功能：	主要负责接收前端查询备份数据传到后端接收的边界类				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | String | keyword | 关键词查询（用户名或者用户ID） |  |
|  | private | String | queryTime | 查询备份时间（模糊查询） |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | String | 无 | getKeyword | 获取关键词 |
|  | public | String | 无 |  getQueryTime | 获取备份时间 |

3.2.3 MaterialVO类说明  
3.2.3.1 类说明表
类名称：	MaterialVo	所在层：	边界层		
主要功能：	主要负责接收前端查询分析数据传到后端接收的边界类				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Integer |  materialId | 存放物质数据Id |  |
|  | private | String | materialType | 存放物质类型 |  |
|  | private | String | materialName | 存放物质名称 |  |
|  | private | Double | materialSolubility | 存放物质溶度 |  |
|  | private | Date | createTime | 存放物质数据创建时间 |  |
|  | private | Date | updateTime | 存放物质数据修改时间 |  |
|  | private | String | bufferSolution | 存放缓冲溶液名称 |  |
|  | private | String | materialRemarks | 存放物质数据备注消息 |  |
|  | private | String | userId | 存放创建物质数据的 用户Id |  |
|  | private | List<Points> | materialPoints | 存放物质数据具体的电流与电位的对应关系 |  |
|  | private | String | points | 物质电流与电位的对应关系 |  |
|  | private | Integer | materialState | 物质数据的状态信息
0：原始数据；
1：数据预处理状态；
2：波形参数分析； |  |
|  | private | Double | materialIp | 物质在溶度中的电流 |  |
|  | private | Double | materialEp | 物质在溶度中的电位 |  |
| 方法： | 访问修饰符
（默认public） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | Integer | 无 | getMaterialId | 获取物质数据id |
|  | public | String | 无 |  getMaterialType | 获取物质类型名称 |
|  | public | String | 无 | getMaterialName | 获取物质名称 |
|  | public | Double | 无 | getMaterialSolubility | 获取物质溶度 |
|  | public | Date | 无 | getCreateTime | 获取物质数据创建时间 |
|  | public | Date | 无 | getUpdateTime | 获取物质数据修改时间 |
|  | public | String | 无 | getBufferSolution | 获取缓冲溶液 |
|  | public | String | 无 | getMaterialRemarks | 获取物质数据备注信息 |
|  | public | String | 无 | getUserId | 获取创建物质数据用户Id |
|  | public | List<Points> | 无 | getMaterialPoints | 获取物质数据据点位数据 |
|  | public | file | 无 | getDataLocation | 获取数据文件位置 |
| 涉及物理表： | 5.2.7 物质数据表 5.2.8物质类型表 5.2.9数据详情表 |  |  |  |  |

3.2.4 ReportVo类说明
3.2.4.1 类说明表
类名称：	ReportVo	所在层：	边界层		
主要功能：	主要负责接收从数据库查询到的实验报告数据并返回给前端，以及接受并封装前端参数				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Integer | reportId | 实验报告id |  |
|  | private | String | userId | 对应用户的id |  |
|  | private | String | reportName | 实验报告名称 |  |
|  | private | Integer | materialId | 对应物质的id |  |
|  | private | String | materialName | 物质名称 |  |
|  | private | String | resultModel | 结果模型的存储地址 |  |
|  | private | String | analysisResultsDiagram | 分析结果图  |  |
|  | private | String | trainsetData | 包含训练集的原数据和预测结果 |  |
|  | private | String | testsetData | 包含测试集的原数据和预测结果 |  |
|  | private | Double | rcSqrt | 训练集决定系数 |  |
|  | private | Double | rmsec | 训练集均方根误差

 |  |
|  | private | Double | maec | 训练集平均绝对误差

 |  |
|  | private | Double | rpSqrt | 测试集决定系数

 |  |
|  | private | Double | rmsep | 测试集均方根误差

 |  |
|  | private | Double | maep | 测试集平均误差

 |  |
|  | private | Double | rpd | 预测误差 |  |
|  | private | String | reportDescribe | 该实验报告的描述和说明 |  |
|  | private | Date | updateTime | 更新时间。精确到毫秒保留3位毫秒数 |  |
|  | private | Date | createDate | 创建时间。精确到毫秒保留3位毫秒数 |  |
| 方法： | 访问修饰符
（默认为public） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | Integer | 无 | getReportId | 获取实验报告id |
|  | public | String | 无 | getUserId | 获取对应用户的id |
|  | public | Integer | 无 | getReportName | 获取实验报告名称 |
|  | public | String | 无 | getMaterialId | 获取对应物质的id |
|  | public | String | 无 | getMaterialName | 获取物质名称 |
|  | public | String | 无 | getResultModel | 获取结果模型的存储地址 |
|  | public | String | 无 | getAnalysisResultsDiagram | 获取分析结果图  |
|  | public | String | 无 | getTrainsetData | 获取包含训练集的原数据和预测结果 |
|  | public | String | 无 | getTestsetData | 获取包含测试集的原数据和预测结果 |
|  | public | Double | 无 | getRcSqrt | 获取训练集决定系数 |
|  | public | Double | 无 | getRmsec | 获取训练集均方根误差

 |
|  | public | Double | 无 | getMaec | 获取训练集平均绝对误差

 |
|  | public | Double | 无 | getRpSqrt | 获取测试集决定系数

 |
|  | public | Double | 无 | getRmsep | 获取测试集均方根误差

 |
|  | public | Double | 无 | getMaep | 获取测试集平均误差

 |
|  | public | Double | 无 | getRpd | 获取预测误差 |
|  | public | String | 无 | getReportDescribe | 获取该实验报告的描述和说明 |
|  | public | Date | 无 | getUpdateTime | 获取创建时间。精确到毫秒保留3位毫秒数 |
|  | public | Date | 无 | getCreateDate | 获取创建时间。精确到毫秒保留3位毫秒数 |
| 涉及物理表： | 5.2.10实验报告表 5.2.11实验报告详情表 |  |  |  |  |

3.2.5 LogVo类说明
3.2.5.1 类说明表
类名称：	LogVo	所在层：	边界层		
主要功能：	主要负责接收从数据库查询到的日志数据并返回给前端，以及接受并封装前端参数				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Integer | logId | 日志id |  |
|  | private | String | logurl | 存放日志的路径 |  |
|  | private | Integer | logState | 日志的状态 |  |
|  | private | String | logContent | 日志的内容 |  |
|  | private | String | userName | 日志对应的用户名 |  |
|  | private | String | handlerIp | 操作日志的用户的ip |  |
|  | private | String | createTime | 日志的创建时间 |  |
|  | private | String | updateTime | 日志的更新时间  |  |
| 方法： | 访问修饰符
（默认为public） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | Integer | 无 | getLogtId | 获取日志报告的id |
|  | public | String | 无 | getLogUrlId | 获取对应日志存储路径 |
|  | public | Integer | 无 | getLogState | 获取对应日志的状态 |
|  | public | String | 无 | getLogContent | 获取对应日志的内容 |
|  | public | String | 无 | getUserName | 获取对应日志对应的用户名 |
|  | public | String | 无 | getHandlerIp | 获取操作日志的用户ip |
|  | public | Date | 无 | getUpdateTime | 获取创建时间。精确到毫秒保留3位毫秒数 |
|  | public | Date | 无 | getCreateTime | 获取创建时间。精确到毫秒保留3位毫秒数 |
| 涉及物理表 |                                                        5.2.14日志表，5.2.1用户表              |  |  |  |  |

3.2.6 AlgorithmVO类说明
3.2.6.1 类说明表
类名称：	AlgorithmVO	所在层：	边界层		
主要功能：	主要负责接收前端上传过来的有关算法的数据传到控制层				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Integer | algorithmId | 算法id |  |
|  | private | String | algorithmName | 算法名称 |  |
|  | private | String | hotNumber | 算法的使用量 |  |
|  | private | String | algorithmDescribe | 算法描述 |  |
|  | private | String | algorithmUrl | 算法存放路径 |  |
|  | private | String | userId | 上传算法的用户 |  |
|  | private | String | algorithmState | 算法状态 |  |
|  | private | String | algorithmType | 算法类型 |  |
|  | private | Interger | boxTest |              算法是否通过黑盒测试 |  |
|  | private | Date | createTime | 算法创建时间 |  |
|  | private | Double | updateTime | 算法更新时间 |  |
| 方法： | 访问修饰符
（默认为public） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | Integer | 无 | getAlgorithmId | 获取算法id |
|  | public | String | 无 | getAlgorithmName |                   获取算法名称 |
|  | public | Integer | 无 | getHotNumber() | 获取算法使用量 |
|  | public | String | 无 | getAlgorithmDecribe() | 获取算法描述 |
|  | public | String | 无 | getAlgorithmUrl() | 获取算法存放路径 |
|  | public | String | 无 | getUserId() | 获取上传算法用户id |
|  | public | Interger | 无 | getAlgorithmState() | 获取算法状态 |
|  | public | String | 无 |  getAlgorithmType() | 获取算法类型 |
|  | public | Interger | 无 | getBoxTest() | 获取通过黑盒测试状态 |
|  | public | Date | 无 | getCreateTime() | 获取上传算法时间 |
|  | public | Date | 无 | getUpdateTime() | 获取算法更新时间 |
| 涉及物理表 |                                         5.2.1用户表,5.2.11算法表 |  |  |  |  |

3.2.7 PreMappedStatement类说明
3.2.7.1 类说明
类名称：	PreMappedStatement	所在层：	边界层		
主要功能：	主要负责将边界层类进行包装以供不同层次数据交流				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | String | methodType | 数据操作名称 |  |
|  | private | 泛型 | data | 边界层视图类 |  |
|  | private | File | file | 用户上传的文件 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | String | 无 | get
MethodType | 获取
数据操作名称 |
|  | public | 泛型 | 无 | getData | 获取视图对象 |
|  | public | File | 无 | getFile | 获取文件 |

3.2.8 RequestVo类说明
3.2.8.1 类说明
类名称：	RequestVo	所在层：	边界层		
主要功能：	主要负责将后端处理之后的数据通过这个类包装成统一类的边界类				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Integer | requestId | 按要求给予申请唯一的Id |  |
|  | private | String | requestContent | 申请理由 |  |
|  | private | Integer | userId | 用户ID |  |
|  | private | Integer | requestType | 请求类型
0：解冻
1：上传算法
2：上传物质类型
3：数据还原 |  |
|  | private | Integer | requestHandlerId | 处理该申请的管理员用户ID |  |
|  | private | Date | createTime | 申请创建账号时间（默认值为精确到秒） |  |
|  | private | Date | updateTime | 申请更新账号时间（默认值为精确到秒） |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | String | 无 | getKeyword | 获取关键词 |
|  | public | String | 无 |  getQueryTime | 获取备份时间 |

3.2.9 ResultEntity类说明
3.2.9.1 类说明
类名称：	ResultEntity	所在层：	边界层		
主要功能：	主要负责将后端处理之后的数据通过这个类包装成统一类的边界类				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Integer | code | 返回状态码 |  |
|  | private | String | message | 返回结果信息 |  |
|  | private | T | data | 存储的数据 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | String | 无 | getKeyword | 获取关键词 |
|  | public | String | 无 |  getQueryTime | 获取备份时间 |

3.2.9 登录界面
3.2.9.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
登录按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	调用登录系统接口
| 用户信息输入框
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 收集用户信息 |

3.2.9.2 界面规约
UTF-8编码

使用中文

3.2.9.3 界面提示信息
提示信息1:请输入邮箱地址!

提示信息2:请输入正确的邮箱地址!

提示信息3:请输入密码!

提示信息4:密码不少于6位!

提示信息5:登陆成功!

提示信息6:您的帐号已冻结!

3.2.10 注册界面
3.2.10.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
注册按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	调用注册系统接口
| 用户信息输入框
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 收集用户信息 |

3.2.10.2 界面规约
UTF-8编码

使用中文

3.2.10.3 界面提示信息
提示信息1:请输入用户名!

提示信息2:用户名的长度必须是2-60位!

提示信息3:请输入手机号!

提示信息4:请输入正确的手机号码!

提示信息5:请输入邮箱地址!

提示信息6:请输入正确的邮箱地址!

提示信息7:请输入验证码!

提示信息8:验证码错误!

提示信息9:请输入密码!

提示信息10:注册成功!

3.2.11 解冻申请界面
3.2.11.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
解冻按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	调用解冻系统接口
| 用户信息输入框
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 收集用户信息 |
| 封禁类型单选框 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 选择封禁类型 |

3.2.11.2 界面规约
UTF-8编码

使用中文

3.2.11.3 界面提示信息
提示信息1:请输入邮箱地址!

提示信息2:请输入正确的邮箱地址!

提示信息3:该用户不存在!

提示信息4:请输入验证码!

提示信息5:验证码错误!

提示信息6:解冻成功!

提示信息7∶提交成功!

3.2.12 忘记密码界面
3.2.12.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
修改密码按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	调用修改密码系统接口
| 用户信息输入框
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 收集用户信息 |

3.2.12.2 界面规约
UTF-8编码

使用中文

3.2.12.3 界面提示信息
提示信息1:该用户不存在!

提示信息2:用户名的长度必须是2-60位!

提示信息3:请输入密码!

提示信息4:密码不少于6位!

提示信息5:请输入邮箱地址!

提示信息6:请输入正确的邮箱地址!

提示信息7:请输入验证码!

提示信息8:验证码错误!

提示信息9:两次密码输入不一致!

提示信息10:修改成功!

3.2.13 首页界面
3.2.13.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
导入数据按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	调用导入数据系统接口
3.2.13.2 界面规约
UTF-8编码

使用中文

3.2.14 数据管理界面
3.2.14.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
导入数据按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	调用导入数据系统接口
| 删除按钮
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 删除数据信息 |
| 搜索框
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 搜索数据信息 |

3.2.14.2 界面规约
UTF-8编码

使用中文

3.2.14.3 界面提示信息
提示信息2:删除成功/失败!

提示信息3:确定要将所有数据删除吗?

3.2.15 导入数据界面
3.2.15.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
浏览数据位置	X:499 Y:706 W:506 H:59	宋体，黑，20px	上传文件
| 提交按钮
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 导入数据 |
| 重置按钮
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 重置输入框信息 |

3.2.15.2 界面规约
UTF-8编码

使用中文

3.2.15.3 界面提示信息
提示信息1:导入成功/失败!

3.2.16 导入数据界面
3.2.16.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
浏览数据位置	X:499 Y:706 W:506 H:59	宋体，黑，20px	上传文件
| 提交按钮
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 导入数据 |
| 重置按钮
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 重置输入框信息 |

3.2.16.2 界面规约
UTF-8编码

使用中文

3.2.16.3 界面提示信息
提示信息1:导入成功/失败!

3.2.17 数据建模界面
3.2.17.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
导入预处理方法按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	上传文件
| 立即创建按钮
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 数据建模 |
| 重置按钮
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 重置输入框信息 |

3.2.17.2 界面规约
UTF-8编码

使用中文

3.2.17.3 界面提示信息
提示信息1:创建成功/失败!

3.2.18 实验报告界面
3.2.18.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
下载按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	下载实验报告文件
| 编辑按钮
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 编辑实验报告 |
| 查看按钮
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 查看实验报告图片 |

3.2.18.2 界面规约
UTF-8编码

使用中文

3.2.18.3 界面提示信息
提示信息1:确定要下载所有数据吗?

提示信息2:删除成功/失败!

提示信息3:确定要将所有数据删除吗?

3.2.19数据预处理界面
3.2.19.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
算法选择按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	选择处理算法
| 波形分析按钮
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 波形分析 |

3.2.19.2 界面规约
UTF-8编码

使用中文

3.2.19.3 界面提示信息
提示信息1:处理成功/失败！

3.2.20波形分析界面
3.2.20.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
保存按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	保存处理后数据
| 还原按钮
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 还原至原数据 |

3.2.20.2 界面规约
UTF-8编码

使用中文

3.2.20.3 界面提示信息
提示信息1:保存成功/失败!

提示信息2:还原成功/失败!

3.2.21算法管理界面
3.2.21.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
开源按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	提交算法开源申请
添加算法按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	添加算法
| 删除按钮
 | X:499 Y:706 W:506 H:59 | 宋体，黑，20px | 删除算法 |

3.2.21.2 界面规约
UTF-8编码

使用中文

3.2.21.3 界面提示信息
提示信息1:添加成功/失败!

提示信息2:删除成功/失败!

提示信息3:确定要将所有数据删除吗?

3.2.22算法申请界面
3.2.22.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
算法包按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	导入算法
提交按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	提交申请
3.2.22.2 界面规约
UTF-8编码

使用中文

3.2.22.3 界面提示信息
提示信息1:申请成功/失败!

3.2.23检测方法界面
3.2.23.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
开始按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	开始检测
重置按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	重置表单信息
3.2.23.2 界面规约
UTF-8编码

使用中文

3.2.24通知界面
3.2.24.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
删除按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	删除通知
3.2.24.2 界面规约
UTF-8编码

使用中文

3.2.24.3 界面提示信息
提示信息1:删除成功/失败!

3.2.25个人信息界面
3.2.25.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
修改按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	修改密码/邮箱
3.2.25.2 界面规约
UTF-8编码

使用中文

3.2.26备份管理界面
3.2.26.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
删除按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	删除备份
修改按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	修改数据信息
3.2.26.2 界面规约
UTF-8编码

使用中文

3.2.26.3 界面提示信息
提示信息1:修改成功/失败!

提示信息2:删除成功/失败!

提示信息3:确定要将所有数据删除吗?

提示信息4:重置成功/失败!

3.2.27用户管理界面
3.2.27.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
删除按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	删除备份
修改按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	修改用户密码
3.2.27.2 界面规约
UTF-8编码

使用中文

3.2.27.3 界面提示信息
提示信息1:修改成功/失败!

提示信息2:删除成功/失败!

提示信息3:确定要将所有数据删除吗?

提示信息4:重置成功/失败!

3.2.28回收站界面
3.2.28.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
还原按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	还原数据
删除按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	彻底删除数据
3.2.28.2 界面规约
UTF-8编码

使用中文

3.2.28.3 界面提示信息
提示信息1:确定要将所有数据彻底删除吗?

提示信息2:确定要还原所有数据吗?

提示信息3:删除成功/失败!

提示信息4:还原成功/失败!

3.2.29物质类型管理界面
3.2.29.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
新增按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	新增物质类型
通过按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	通过申请
驳回按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	驳回申请
3.2.29.2 界面规约
UTF-8编码

使用中文

3.2.29.3 界面提示信息
提示信息1:添加成功/失败!

提示信息2:删除成功/失败!

提示信息3:确定要将所有数据删除吗?

提示信息4:已通过!

提示信息5:已驳回!

3.2.30权限管理界面
3.2.30.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
新增按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	新增权限
删除按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	删除权限
3.2.30.2 界面规约
UTF-8编码

使用中文

3.2.30.3 界面提示信息
提示信息1:修改成功/失败!

提示信息2:删除成功/失败!

提示信息3:确定要将所有数据删除吗?

3.2.31日志管理界面
3.2.31.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
导出按钮	X:499 Y:706 W:506 H:59	宋体，黑，20px	导出日志表格
3.2.31.2 界面规约
UTF-8编码

使用中文

3.2.31.3 界面提示信息
提示信息1:导出成功/失败!

3.2.32导航栏
3.2.32.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
侧边导航栏	X:499 Y:706 W:506 H:59	宋体，黑，20px	根据不同角色显示不同导航栏
3.2.32.2 界面规约
UTF-8编码

使用中文

3.2.33表格
3.2.33.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
表格	X:499 Y:706 W:506 H:59	宋体，黑，20px	根据不同数据显示不同表格
3.2.33.2 界面规约
UTF-8编码

使用中文

3.2.34快捷操作
3.2.34.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
快捷操作	X:499 Y:706 W:506 H:59	宋体，黑，20px	可以快捷进入数据分析，数据管理，等界面
3.2.34.2 界面规约
UTF-8编码

使用中文

3.2.35图表
3.2.35.1 界面组件说明
组件名称	组件位置	字体样式及大小	组件作用
图表	X:499 Y:706 W:506 H:59	宋体，黑，20px	根据不同数据显示不同曲线图
3.2.35.2 界面规约
UTF-8编码

使用中文

3.3控制类
3.3.1 类说明表
类名称：	Controller	所在层：	控制层		
主要功能：	主要负责接收前端请求、分发方法、包装参数				
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | HttpResponse | HttpRequest | orininal | 分发请求给对应的服务类，处理后接收返回值包装成JSON数据返回前端 |
| 参数说明： | 方法名称 |  | 参数说明 |  |  |
|  | service |  | HttpRequest：请求协议包
HttpResponse：响应协议包 |  |  |

3.3.2 类作用流程图

图 控制类orininal方法流程图

3.4应用层
3.4.1 UserServiceImpl类说明
3.4.1.1 类说明表
类名称：	UserServiceImpl	所在层：	应用层		
主要功能：	主要负责实现用户数据相关业务				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | util | UtilTool | 查询系统数据类 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | private | String | 无 | userLogin | 用户登录业务方法，需要用户账号、密码 |
|  | private | String | String，String | applyForUnfreeze | 用户申请业务方法，需要用户账号，申请理由，和申请类型 |
|  | private | String | 无 | userForgetPassword | 用户忘记密码方法，需要账号和验证码 |
|  | private | String | 无 | addOneUser | 用户注册方法，注册新账号，需要行的user对象 |
|  | private | String | 无 | userUpdate | 用户修改个人信息方法 |
|  | public | 对象 | PreMappedStatement | UserServiceImpl | 构造方法充当业务执行器 |
| 涉及物理表： | 5.2.1用户表、5.2.2权限表、5.2.3角色表、5.2.6申请订单物理表 |  |  |  |  |
| 参数说明： | 方法名称 |  | 参数具体说明 |  |  |
|  | applyForUnfreeze |  | 第一个String类型参数: 用户申请理由信息；
第二个String类型参数：申请类型； |  |  |

3.4.1.2 类作用流程图

图 UserServiceImpl类userLogin方法流程图

图 UserServiceImpl类applyForUnfreeze方法流程图

图 UserServiceImpl类userForgetPassword方法流程图

图 UserServiceImpl类构造方法流程图

3.4.2 BackupsServiceImpl类说明
3.4.2.1 类说明表
类名称：	BackupsServiceImpl	所在层：	应用层		
主要功能：	主要负责实现备份数据相关业务				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Data | data | 查询系统数据类 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | 对象 | PreMappedStatement | BackupsServiceImpl | 构造方法充当业务执行器 |
|  | private | ResultEntity<T> | 无 | backups | 备份当前系统数据 |
|  | private | ResultEntity<T> | 无 | backupsPeriod | 设置系统备份周期 |
|  | private | ResultEntity<T> | 无 | restoreData | 还原指定用户数据 |
|  | private | ResultEntity<T> | 无 | queryBackups | 获取全部的部分数据和自动备份周期 |
|  | private | 无 | 无 | delete | 通过用户Id列表删除指定数据 |
| 参数说明： | 方法名称 |  | 参数具体说明 |  |  |
|  | BackupsServiceImpl构造方法 |  | PreMappedStatement类型参数 里面包路由  泛型数据 泛型数据包括下面三种方法的参数类型 |  |  |
|  | backups |  | 无 |  |  |
|  | backupsPeriod |  | Integer类型的backupsPeriod参数 设置周期时间 |  |  |
|  | restoreData |  | 参数：List<Integer> 类型作为还原用户的列表Id |  |  |

说明：实现类中的全部私有方法的参数通过获取父类中的PreMappedStatement参数。

3.4.2.2 类作用流程图

图 BackupsServiceImpl类backups方法流程图

图 BackupsServiceImpl类restoreData方法流程图

图  BackupsServiceImpl类构造方法流程图

3.4.3 AnalyzeDataServiceImpl类说明
3.4.3.1 类说明表
类名称：	AnalyzeDataServiceImpl	所在层：	应用层		
主要功能：	主要负责实现分析数据相关业务				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Data | data | 查询系统数据类 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | private | List<Points> | List<Points>,String,Integer | doPreprocessData | 用户预处理数据 |
|  | private | String | InputStream File |  checkDataFormat | 检测用户导入数据文件是否合格 |
|  | private | MaterialVO | MaterialVO | doWaveformAnalysis | 把数据波形分析数据保存到数据库中 |
|  | private | bool | List<MaterialVO> | creatOneReport | 数据建模得到实验报告 |
|  | private | Process | String[] | execAlgorithm | 执行预处理/波形参数分析算法 |
|  | private | bool | MaterialVO | importAnalyzeData | 用户导入实验数据 |
|  | private | MaterialVO | Integer,Integer | doQueryPoints | 查询实验数据 |
|  | public | 对象 | PreMappedStatement | AnalyzeDataServiceImpl | 构造方法充当业务执行器 |
|  | private | List | MaterialVO | WaveformAnalysis | 实际进行波形参数分析 |
| 涉及物理表 | 5.2.7物质数据表、5.2.8物质类型表、5.2.9数据详情表、5.2.10实验报告表、5.2.12算法表 |  |  |  |  |
| 参数说明 | 方法名称 |  | 参数说明 |  |  |
|  | doPreprocessData |  | 第一个参数表示实验分析数据点的集合；第二个参数表示预处理算法名称；第三个参数表示物质数据的id |  |  |
|  | creatOneReport |  | 参数表示多条实验数据 |  |  |
|  | execAlgorithm |  | 第一个参数表示为算法的环境，第二个参数为算法的路径，第三个参数表示为调用算法的参数 |  |  |
|  | doQueryPoints |  | 第一个参数表示物质数据的id；第二个表示数据的状态（0：原始数据；1：数据预处理状态；2：波形参数分析；3：删除状态；） |  |  |

3.4.3.2 类作用流程图

图 AnalyzeDataServiceImpl类doImportData方法流程图


图 AnalyzeDataServiceImpl类checkDataFormat方法流程图


图 AnalyzeDataServiceImpl类doPreprocessData方法流程图


图 AnalyzeDataServiceImpl类creatOneReport方法流程图

图 AnalyzeDataServiceImpl类doService方法流程图


图 AnalyzeDataServiceImpl类AnalyzeDataServiceImpl方法流程图

3.4.4 DataChargeServiceImpl类说明
3.4.4.1 类说明表
类名称：	dateChargeServiceImpl	所在层：	应用层		
主要功能：	主要负责管理物质数据、实验报告和物质类型相关业务				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Data | data | 查询系统数据类 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | 对象 | PrepareStatement | DataChargeServiceImpl | 构造方法充当业务执行器 |
|  | private | List<MaterialVo> | Material | selectMaterial | （批量）搜索个人物质数据信息 |
|  | private | byte[] | Interger | exportData | 导出物质数据 |
|  | private | boolean | List<Interger> | logicalDeleteMaterial | 逻辑删除物质数据 |
|  | private | boolean | List<Interger> | deleteMaterialForever | 彻底删除物质数据 |
|  | private | boolean | List<Interger> | undoDeleteMaterial | 从回收站中取消删除物质数据 |
|  | private | boolean | RequestVo | applyRestoreMaterials | 申请恢复物质数据 |
|  | private | List<RequestVo> | void | selectUserMaterialsRequests | 展示用户物质数据恢复申请 |
|  | private | boolean | Integer | restoreUserMaterials | 恢复用户物质数据 |
|  | private | boolean | Integer | adjustRecycle | 调整回收站时间 |
|  | private | List<Report> | Report | selectReport | 查询实验报告 |
|  | private | boolean | Report | updateReport | 修改实验报告 |
|  | private | boolean | List<Interger> | logicalDeleteReport | 逻辑删除实验报告 |
|  | private | boolean | List<Interger> | deleteReportForever | 彻底删除实验报告 |
|  | private | boolean | List<Interger> | undoDelectReport | 从回收站中取消删除实验报告 |
|  | private | byte[] | Integer | exportReport | 导出实验报告 |
|  | private | boolean | RequestVo | applyAddMaterialType | 用户申请添加物质类型 |
|  | private | boolean | MaterialType | addMaterialType | 管理员添加物质类型 |
|  | private | boolean | List<Integer> | deleteMaterialType | 管理员删除物质类型 |
|  | private | boolean | MaterialType | updateMaterialType | 管理员修改物质类型 |
|  | private | List<MateriaiType> | List<Integer> | selectMaterialType  | 管理员查询物质类型 |
|  | private | List<ReportVo> | 无 | selectMaterialTypeRequests | 管理员查询物质类型申请列表 |
|  | private | boolean | boolean | examineMaterialTypeRequests | 管理员审核物质类型申请 |
| 涉及物理表： | 5.2.6申请订单表 5.2.7物质数据表 5.2.8物质类型表 5.2.9数据详情表  5.2.10实验报告表 5.2.11实验报告详情表 |  |  |  |  |
| 参数说明 | 方法名称 | 具体参数说明 |  |  |  |
|  | selectMaterial | 物质的信息（物质类型、物质名称、id） |  |  |  |
|  | exportData | 物质数据id |  |  |  |
|  | applyRestoreMaterials | Request:用户填写的申请信息类 |  |  |  |
|  | selectUserMaterialsRequests | 用户id |  |  |  |
|  | adjustRecycle | 调整的时间（天） |  |  |  |
|  | examineMaterialTypeRequests | 值为ture代表申请通过，false为申请不通过 |  |  |  |

3.4.4.2 类作用流程图

图dateChargeServiceImpl类exportData方法流程图

图dateChargeServiceImpl类restoreReport方法流程图

3.4.5 DataServiceImpl类说明
3.4.5.1 类说明表
类名称：	DataServiceImpl	所在层：	应用层		
主要功能：	主要负责实现查询相关业务				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Data | data | 查询系统数据类 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  |  public | 对象 | 无 | DataServiceImpl | 构造方法、执行方法 |
|  | private | List<Object> | 无 | query | 统一查询方法 |
|  | private | int | 无 | insert | 统一插入方法 |
|  | private | int | 无 | delect | 统一删除方法 |
|  | private | int | 无 | updata | 统一修改方法 |

3.4.6LogServiceImpl类说明
3.4.6.1 类说明表
类名称：	LogServiceImpl	所在层：	应用层		
主要功能：	主要负责实现日志相关业务				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  |           protect |        Util  |             util |                    工具类 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | private |      File | Integer[] | exportLogs | 根据所传日志id导出日志为excel文件 |
|  |            private |    List<Log> |         String |        getLogs |  根据用户名称查询对应得用户日志 |
|  |            private |    List<Log> |         无 |        getAllLog | 查询所有用户得日志 |
|  |            private |    int |         Log |        addLog | 插入用户的日志 |
|  | public | 对象 | PreMappedStatement | LogServiceImp | 构造方法执行器 |
|  |                                       5.2.1用户表，5.2.14日志表 |  |  |  |  |
| exportLogs方法参数说明：第一个位置上为前端传入得id整形数组，
addLogs方法参数说明：第一个位置上为封装好了得Log对象.
getLogs方法参数说明：第一个位置上为用户得用户名，根据用户名进行查询日志对应的日志内容 |  |  |  |  |  |

3.4.6.2 类作用流程图

                                                                 图 LogServiceImpl类exportLogs方法流程图

3.4.7AgrithmServiceImpl
3.4.7.1类说明表
类名称：	AgrithmServiceImpl	所在层：	应用层		
主要功能：	主要负责实现算法管理相关业务				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Data | data | 查询系统数据类 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | private | int | 
 | addAlgotithm | 用户上传算法,需要上传算法文件 |
|  | private | int | 
 | deleteAlgotithm | 管理员删除算法,需要算法id |
|  | private | Algorithm | 
 |  searchAlgorithm | 搜索算法,需要算法id |
|  | private | Algorithm | 
 | updateAlgorithm |  更新算法,需要算法id |
|  | public | 对象 | PreMappedStatement | AgrithmServiceImpl | 构造方法充当业务执行器 |
| 参数说明 | 方法名称: addAlgotithm |  |  | 用户上传所需算法包 |  |
|  | 方法名称:deleteAlgotithm |  |  | 删除算法所需id |  |
|  | 方法名称: searchAlgotithm |  |  | 搜索算法所需id |  |
|  | 方法名称: updateAlgorithm |  |  | 更新算法所需id |  |
| 涉及物理表 |                                   5.2.1用户表,5.2.11算法表 |  |  |  |  |

3.4.7.2类作用流程图

                                     图AgririthmServiceImpl类addAgrithm方法活动流程图

3.4.8 MessageQueue
3.4.8.1 类说明表
类名称：	AgrithmServiceImpl	所在层：	应用层		
主要功能：	主要负责实现算法管理相关业务				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | protected | Queue
<PreMappedStatement<String>> | requestQueue | 请求队列 |  |
|  | protected | Queue
<ResultEntity<T>> | responseQueue | 响应队列 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | protection | ResultEntity<T> | 无 | romveQueue | 队列元素出队 |
|  | protection | Boolean | PreMappedStatement | addQueue | 元素入队 |
| 参数说明： | 方法名称 |  | 参数具体说明 |  |  |
|  | addQueue |  | 第一个参数：PreMappedStatement内泛型数据是JSON数据串。 |  |  |

3.4.8.2 类作用活动流程图

图 MessageQueue类romveQueue方法活动图

图 MessageQueue类addQueue方法活动图

3.4.9 DistributeClass
3.4.9.1 类说明表
类名称：	AgrithmServiceImpl	所在层：	应用层		
主要功能：	主要负责实现算法管理相关业务				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | protected | PreMappedStatement | preMappedStatement | 参数包装类，交由子类使用 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | ResultEntity<T> | PreMappedStatement | update | 分发请求到各个执行器 |
| 参数说明： | 方法名称 |  | 参数具体说明 |  |  |
|  | update |  | 第一个参数：PreMappedStatement内泛型数据是JSON数据串。 |  |  |

3.4.9.2 类作用活动流程说明

图 DistributeClass类update方法活动图

3.4.10 UtilTool
3.4.10.1 类说明表
类名称：	UtilTool	所在层：	应用层		
主要功能：	为业务层方法提供便捷方法，和操作数据库接口				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Data | data | 数据库操作类 |  |
| 静态方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | protected | boolean | String | sendMessage | 发送验证码 |
|  | protected | boolean | String | blackBoxTesting | 黑盒测试 |
|  | protected | ResultEntity<T> | PreMappedStatement | userAlgorithm | 使用算法 |
| 方法： | protected | List<Object> | PreMappedStatement | query | 查询工具方法 |
|  | protected | int | PreMappedStatement | insert | 插入工具方法 |
|  | protected | int | PreMappedStatement | delete | 删除工具方法 |
|  | protected | int | PreMappedStatement | updata | 修改工具方法 |

3.4.10.2 类作用活动流程说明

图 UtilTool sendMessage方法流程图

3.5数据层
3.5.1数据操作类
3.5.1.1 Data类
3.5.1.1.1 类说明表
类名称：	Data	所在层：	数据层		
主要功能：	主要负责实现分发数据操作的相关方法				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | HashMap | map | 存放mapper接口的全限定类名 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | 泛型 | 泛型 | query | 数据查询操作 |
|  | public | 泛型 | 泛型 | update | 数据修改操作 |
|  | public | 泛型 | 泛型 | insert | 数据插入操作 |

3.5.1.1.2 类作用流程图

图 DataImpl类query方法流程图

3.5.1.2 DataQueue
3.5.1.2.1 类说明表
类名称：	DataQueue	所在层：	数据层		
主要功能：	存放数据操作请求队列				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Queue
<PreMappedStatement> | dataQueue | 数据操作请求队列 |  |
|  | private | Queue
<PreMappedStatement> | returnQueue | 数据结果队列 |  |
|  | private | Data | data | dataQueue监视器 |  |
|  | private | SQL | sql | sql监视器 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | Boolean | PreMappedStatement | add | 插入队列操作 |
|  | public | PreMappedStatement | 无 | remove | 元素出队操作 |

3.5.1.3 SQL
3.5.1.3.1 类说明表
类名称：	SQL	所在层：	数据层		
主要功能：	拼接SQL语句，执行SQL操作				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | String | sql | SQL语句 |  |
|  | private | HashMap<Integer,String> | tableMap | 视图表映射，每一个映射对应一段SQL片段 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | String | PreMappedStatement | update | 队列监听 |
|  | private | String | PreMappedStatement | PreMappedStatement | 构建SQL语句 |
|  | private | 泛型 | String | doSql | 执行SQL语句 |

3.5.1.3.2 类作用活动流程图

图 SQL类sql方法流程图

3.5.2数据实体类
3.5.2.1 User类
3.5.2.1.1 类说明表
类名称：	User	所在层：	数据层		
主要功能：	对实现世界用户抽象出的用户类				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | String | id  | 存放用户ID |  |
|  | private | String | userName | 代表用户名称 |  |
|  | private | String | userPassword | 存放用户密码 |  |
|  | private | String | userSex | 存放用户性别 |  |
|  | private | String | userPhone | 存放用户名称 |  |
|  | private | String | userAvatar | 存放用户头像 |  |
|  | private | String | userEmail | 存放用户邮箱 |  |
|  | private | Integer | userStatus | 表示用户状态 |  |
|  | private | List<Role> | userRole | 存放用户所有角色 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | String | 无 | getUserName | 获取用户名称 |
|  | public | void | String | setUserName | 设置用户名称 |
|  | public | String | 无 |  getUserSex | 获取用户性别 |
|  | public | void | String | setUserSex | 设置用户性别 |
|  | public | String | 无 | getUserPhone | 获取用户电话号码 |
|  | public | void | String | setUserPhone | 设置用户电话号码 |
|  | public | String | 无 | getUserAvatar | 获取用户头像地址链接 |
|  | public | void | String | setUserAvatar | 设置用户头像地址链接 |
|  | public | String | 无 | getUserEmail | 获取用户邮箱 |
|  | public | void | String | setUserEmail | 设置用户邮箱 |
|  | public | Integer | 无 | getUserStatus | 获取用户状态 |
|  | public | List<Role> | 无 | getUserRoles | 获取用户角色 |
|  | public | void | List<Role> | setUserRoles | 设置用户角色 |

3.5.2.2 Backups类
3.5.2.2.1 类说明
类名称：	Backups	所在层：	数据层		
主要功能：	对系统备份系统数据抽象的实体类				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Integer | backupsId | 存放备份ID |  |
|  | private | String | userId | 代表用户ID |  |
|  | private | String | username | 存放用户名称 |  |
|  | private | String | backupsRemark | 存放备份备注 |  |
|  | private | Integer | backupsTime | 存放备份备份时间 |  |
| 方法： | 访问修饰符
（默认private） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | Integer | 无 | getBackupsId | 获取部分ID |
|  | public | void | Integer | setBackupsId | 设置用户ID |
|  | public | String | 无 |  getUserId | 获取用户ID |
|  | public | void | String | setUserId | 设置用户ID |
|  | public | String | 无 | getUsername | 获取用户名称 |
|  | public | void | String | setUsername | 设置用户名称 |
|  | public | String | 无 | getBackupsRemark | 获取部分描述 |
|  | public | void | String | setBackupsRemark | 设置备份描述 |
|  | public | Date | 无 | getBackupsTime | 获取备份时间 |
|  | public | void | Date | setBackupsTime | 设置备份时间 |

3.5.2.3 Material类
3.5.2.3.1 类说明
类名称：	Material	所在层：	数据层		
主要功能：	主要负责接收前端查询分析数据传到后端接收的边界类				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Integer |  materialId | 存放物质数据Id |  |
|  | private | String | materialType | 存放物质类型 |  |
|  | private | String | materialName | 存放物质名称 |  |
|  | private | Double | materialSolubility | 存放物质溶度 |  |
|  | private | Date | createTime | 存放物质数据创建时间 |  |
|  | private | Date | updateTime | 存放物质数据修改时间 |  |
|  | private | String | bufferSolution | 存放缓冲溶液名称 |  |
|  | private | String | materialRemarks | 存放物质数据备注消息 |  |
|  | private | String | userId | 存放创建物质数据的用户Id |  |
|  | private | String | materialPoints | 存放物质数据具体的电流与电位的对应关系 |  |
| 方法： | 访问修饰符
（默认public） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | Integer | 无 | getMaterialId | 获取物质数据id |
|  | public | void | Integer | setMaterialId | 设置
获取物质数据id |
|  | public | String | 无 |  getMaterialType | 获取物质类型名称 |
|  | public | void | String | setMaterialType | 设置物质类型名称 |
|  | public | String | 无 | getMaterialName | 获取物质名称 |
|  | public | void | String | setMaterialName | 设置物质名称 |
|  | public | Double | 无 | getMaterialSolubility | 获取物质溶度 |
|  | public | void | Double | setMaterialSolubility | 设置物质溶度 |
|  | public | Date | 无 | getCreateTime | 获取物质数据创建时间 |
|  | public | void | Date | setCreateTime | 设置物质数据创建时间 |
|  | public | Date | 无 | getUpdateTime | 获取物质数据修改时间 |
|  | public | void | Date | setUpdateTime | 设置物质数据修改时间 |
|  | public | String | 无 | getBufferSolution | 获取缓冲溶液 |
|  | public | void | String | setBufferSolution | 设置缓冲溶液 |
|  | public | String | 无 | getMaterialRemarks | 获取物质数据备注信息 |
|  | public | void | String | setMaterialRemarks | 设置物质数据备注信息 |
|  | public | String | 无 | getUserId | 获取创建物质数据用户Id |
|  | public | void | String | setUserId | 设置创建物质数据用户Id |
|  | public | String | 无 | getMaterialPoints | 获取物质数据据点位数据 |
|  | public | void | String | setMaterialPoints | 设置物质数据据点位数据 |

3.5.3.3 MaterialDeatail类
3.5.2.3.1 类说明
类名称：	MaterialDeatail	所在层：	数据层		
主要功能：	主要负责接收前端查询分析数据传到后端接收的边界类				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Integer | materialDetailId | 按要求给予数据详情唯一的Id |  |
|  | private | String | algorithmName | 数据预处理算法名称（限定10个汉字） |  |
|  | private | String | algorithmId | 预处理算法的ID |  |
|  | private | DOUBLE | materialIp | 波形参数分析中得出的电流（精确到小数点后三位） |  |
|  | private | DOUBLE | materialEp | 波形参数分析中得出的电位值（精确到小数点后三位） |  |
|  | private | Integer | materilState | 0：原始数据；
1：数据预处理状态；
2：波形参数分析； |  |
|  | private | String | leftPoint | 左点位 |  |
|  | private | String | rightPoint | 右点位 |  |
|  | private | String | highPoint | 最高点位 |  |
|  | private | Integer | materialId | 对应物质表中的Id |  |
| 方法： | 访问修饰符
（默认public） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | Integer | 无 | getMaterialDetailId | 获取物质详情数据id |
|  | public | void | Integer | setMaterialDetailId | 设置
获取物质详情数据id |
|  | public | String | 无 |  getAlgorithmName | 获取预处理算法名称 |
|  | public | void | String | setAlgorithmName | 设置预处理算法名称 |
|  | public | String | 无 | getMaterialName | 获取物质名称 |
|  | public | void | String | setMaterialName | 设置物质名称 |
|  | public | String | 无 | getAlgorithmId | 获取预处理算法id |
|  | public | void | String | setAlgorithmId | 设置预处理算法id |
|  | public | Double | 无 | getMaterialIp | 获取波形分析得出中的ip |
|  | public | void | Double | setMaterialIp | 设置波形分析得出中的ip |
|  | public | Double | 无 | getMaterialEp | 获取波形分析得出中的ep |
|  | public | void | Double | setMaterialEp | 设置波形分析得出中的ep |
|  | public | Integer | 无 | getMaterialState | 获取物质状态 |
|  | public | void | Integer | setMaterialState | 设置物质状态 |
|  | public | String | 无 | getLeftPoint | 获取左点位 |
|  | public | void | String | setLeftPoint | 设置左点位 |
|  | public | String | 无 | getRightPoint | 获取右点位 |
|  | public | void | String | setRightPoint | 设置右点位 |
|  | public | String | 无 | getHighPoint | 获取最高点位 |
|  | public | void | String | setHighPoint | 设置最高点位 |
|  | public | Integer | 无 | getMaterialId | 获取物质数据id |
|  | public | void | Integer | setMaterialId | 设置
获取物质数据id |

3.5.3.4 Report类
3.5.2.3.1 类说明
类名称：	Report	所在层：	数据层		
主要功能：	存储实验报告基本数据的实体类				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Integer | materialDetailId | 实验报告唯一的Id |  |
|  | private | String | userId | 对应用户的id |  |
|  | private | String | reportName | 该实验报告的名称（标题） |  |
|  | private | Integer | materialId | 对应物质的id |  |
|  | private | String | materialName | 对应物质的名称 |  |
|  | private | Date | createTime | 实验报告创建时间 |  |
|  | private | Date | updateTime | 实验报告最新更新时间 |  |
|  | private | Integer | reportState | 实验报告存在状态 |  |
|  | private | String | reportDescribe | 该实验报告的描述和说明 |  |
| 方法： | 访问修饰符
（默认public） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | Integer | 无 | getReportId | 获取实验报告id |
|  | public | String | 无 | getUserId | 获取对应用户的id |
|  | public | String | 无 | getReportName | 获取该实验报告的名称（标题） |
|  | public | Integer | 无 | getMaterialId | 获取对应物质的id |
|  | public | String | 无 | getMaterialName | 获取对应物质的名称 |
|  | public | Date | 无 | getCreateTime | 获取实验报告创建时间 |
|  | public | Date | 无 | getUpdateTime | 获取实验报告最新更新时间 |
|  | public | Integer | 无 | getReportState | 获取实验报告存在状态 |
|  | public | String | 无 | getReportDescribe | 获取该实验报告的描述和说明 |
|  | public | void | Integer | setReportId | 设置实验报告id |
|  | public | void | String | setUserId | 设置对应用户的id |
|  | public | void | String | setReportName | 设置该实验报告的名称（标题） |
|  | public | void | Integer | setMaterialId | 设置对应物质的id |
|  | public | void | String | setMaterialName | 设置对应物质的名称 |
|  | public | void | Date | setCreateTime | 设置实验报告创建时间 |
|  | public | void | Date | setUpdateTime | 设置实验报告最新更新时间 |
|  | public | void | Integer | setReportState | 设置实验报告存在状态 |
|  | public | void | String | setReportDescribe | 设置该实验报告的描述和说明 |

3.5.3.4 ReportDetail类
3.5.2.3.1 类说明
类名称：	ReportDetail	所在层：	数据层		
主要功能：	存储实验报告具体内容数据的实体类				
| 属性： | 访问修饰符
（默认private） | 属性类型 | 属性名称 | 功能说明 |  |
|  | private | Integer | materialDetailId | 按要求给予数据详情唯一的Id |  |
|  | private | String | resultModel | 结果模型的存储地址 |  |
|  | private | String | analysisResultsDiagram | 分析结果图  |  |
|  | private | String | trainsetData | 包含训练集的原数据和预测结果 |  |
|  | private | String | testsetData | 包含测试集的原数据和预测结果 |  |
|  | private | Double | rcSqrt | 训练集决定系数 |  |
|  | private | Double | rmsec | 训练集均方根误差

 |  |
|  | private | Double | maec | 训练集平均绝对误差

 |  |
|  | private | Double | rpSqrt | 测试集决定系数

 |  |
|  | private | Double | rmsep | 测试集均方根误差

 |  |
|  | private | Double | maep | 测试集平均误差

 |  |
|  | private | Double | rpd | 预测误差 |  |
| 方法： | 访问修饰符
（默认public） | 方法返回值 | 方法参数 | 方法名称 | 功能说明 |
|  | public | Integer | 无 | getReportId | 获取实验报告id |
|  | public | String | 无 | getResultModel | 获取结果模型的存储地址 |
|  | public | String | 无 | getAnalysisResultsDiagram | 获取分析结果图  |
|  | public | String | 无 | getTrainsetData | 获取包含训练集的原数据和预测结果 |
|  | public | String | 无 | getTestsetData | 获取包含测试集的原数据和预测结果 |
|  | public | Double | 无 | getRcSqrt | 获取训练集决定系数 |
|  | public | Double | 无 | getRmsec | 获取训练集均方根误差

 |
|  | public | Double | 无 | getMaec | 获取训练集平均绝对误差

 |
|  | public | Double | 无 | getRpSqrt | 获取测试集决定系数

 |
|  | public | Double | 无 | getRmsep | 获取测试集均方根误差

 |
|  | public | Double | 无 | getMaep | 获取测试集平均误差

 |
|  | public | Double | 无 | getRpd | 获取预测误差 |
|  | public | String | 无 | getReportDescribe | 获取该实验报告的描述和说明 |
|  | public | void | String | setResultModel | 设置结果模型的存储地址 |
|  | public | void | String | setAnalysisResultsDiagram | 设置分析结果图  |
|  | public | void | String | setTrainsetData | 设置包含训练集的原数据和预测结果 |
|  | public | void | String | setTestsetData | 设置包含测试集的原数据和预测结果 |
|  | public | void | Double | setRcSqrt | 设置训练集决定系数 |
|  | public | void | Double | setRmsec | 设置训练集均方根误差

 |
|  | public | void | Double | setMaec | 设置训练集平均绝对误差

 |
|  | public | void | Double | setRpSqrt | 设置测试集决定系数

 |
|  | public | void | Double | setRmsep | 设置测试集均方根误差

 |
|  | public | void | Double | setMaep | 设置测试集平均误差

 |
|  | public | void | Double | setRpd | 设置预测误差 |

四、系统总体设计书
4.1 总体设计概述
本系统面向对象的思想，将系统依据MVC架构分为三层具体架构图可看图一系统架构。层与层之间我们采取队列进行交互，通过观察者模式将队列改变通知监控类，通过监控类分发请求到执行器。

4.2 系统模块设计
4.2.1 用户管理模块
4.2.1.1 模块说明
系统管理模块，把有关系统的管理功能抽取出来成为一个模块，主要功能有用户登录、用户注册、用户修改个人信息、用户申请等。

4.2.1.2 涉及具体类
本模块涉及类如图所示：

图 用户管理模块涉及图

4.2.1.3 涉及视图
| 序号 | 字段名称 | 类型 | 是否为主键 | 是否能为空 | 默认值 | 描述 |
| --- | --- | --- | --- | --- | --- | --- |
| 1 | user_id | VARCHAR(8) | 是 | 否 | ID为8为字符前四位为年月，后4位为自增
例：22070001 | 用于辨别不同用户，其取值要按要求取值 |
| 2 | user_name | VARCHAR(12) | 否 | 否 | “” | 用户名，由用户自己提供（只能有汉字和英语字符） |
| 3 | user_password  | VARCHAR(15) | 否 | 否 | “Aa1234567?” | MD的用户密码。（大小字符加数字加特殊字符） |
| 4 | user_sex | VARCHAR（3） | 否 | 否 | 男 | 用户的性别男/女（存汉字） |
| 5 | user_phone | VARCHAR(11) | 否 | 否 | "" | 用户的电话（手机号和座机号）有0开始的就是座机号 |
| 6 | user_avatar | VARCHAR(100) | 否 | 否 | "" | 用户的头像链接（OSS存放） |
| 7 | user_email | VARCHAR(20) | 否 | 否 | "" | 用户邮箱，用来登录和找回账号 |
| 8 | user_status | INT(1) | 否 | 否 | 0 |  0：正常使用
1：非法登录冻结状态；
2：非法操作冻结状态；
3：逻辑删除状态
4：禁用 |
| 9 | create_time | DATE | 否 | 否 | NULL | 用户创建账号时间（默认精确到秒） |
| 9 | update_time | DATE | 否 | 否 | NULL | 用户更新账号时间（默认值为精确到秒） |

表 用户视图表

| 序号 | 字段名称 | 类型 | 是否为主键 | 是否能为空 | 默认值 | 描述 |
| --- | --- | --- | --- | --- | --- | --- |
| 1 | permissions_id | INT(2) | 是 | 否 | 自增
例：01 | 按要求给予权限唯一的Id |
| 2 | permissions_name | VARCHAR(18) | 否 | 否 | “数据分析模块” | 角色所能够使用的权限模块名称 |
| 3 | permissions_url | VARCHAR(32) | 否 | 否 | “\index” | 本权限拦截器放行路径 |

表 权限视图表

| 序号 | 字段名称 | 类型 | 是否为主键 | 是否为空 | 默认值 | 描述 |
| --- | --- | --- | --- | --- | --- | --- |
| 1 | role_id | INT(2) | 是 | 否 | 自增
例：01 | 按要求给予角色唯一的Id |
| 2 | role_name | VARCHAR(10) | 否 | 否 | “” | 角色名称（“研究员”，“管理员”，“超级管理员”） |
| 3 | role_content | VARCHAR(255) | 否 | 否 | “” | 角色具体描述（主要描述角色含有的模块权限且大致的操作权限） |

表 角色视图表

| 序号 | 字段名称 | 类型 | 是否为主键 | 是否能为空 | 默认值 | 描述 |
| --- | --- | --- | --- | --- | --- | --- |
| 1 | request_id | INT(2) | 是 | 否 | 自增
例：01 | 按要求给予申请唯一的Id |
| 2 | request_content | VARCHAR(255) | 否 | 否 | "" | 申请理由 |
| 3 | user_id | VARCHAR(8) | 否 | 否 | "" | 用户ID |
| 4 | request_type | INT(2) | 否 | 否 | "" | 请求类型
0：解冻
1：上传算法
2：上传物质类型
3：数据还原 |
| 5 | request_handler_id | VARCHAR(8) | 否 | 否 | "" | 处理该申请的管理员用户ID |
| 5 | create_time | DATE | 否 | 否 | NULL | 申请创建账号时间（默认值为精确到秒） |
| 6 | update_time | DATE | 否 | 否 | NULL | 申请更新账号时间（默认值为精确到秒） |

4.2.1.4 涉及物理表
涉及物质表在本文档第五部分的5.2.1~5.2.6表

4.2.1.5 模块活动图

图 用户登录说明图

图 用户忘记密码说明图
图 用户申请说明图

4.2.2算法管理模块
4.2.2.1模块说明
算法管理模块,管理预处理和数据建模所需要的算法,用户可以提交自己的算法给管理员进行审核,管理员对上传的算法进行私有或则公开

4.2.2.2涉及具体类


4.2.2.3涉及视图
| 序号 | 字段名称 | 类型 | 是否为主键 | 是否能为空 | 默认值 | 描述 |
| --- | --- | --- | --- | --- | --- | --- |
| 1 | user_id | VARCHAR(8) | 是 | 否 | ID为8为字符前四位为年月，后4位为自增
例：22070001 | 用于辨别不同用户，其取值要按要求取值 |
| 2 | user_name | VARCHAR(12) | 否 | 否 | “” | 用户名，由用户自己提供（只能有汉字和英语字符） |
| 3 | user_password  | VARCHAR(15) | 否 | 否 | “Aa1234567?” | MD的用户密码。（大小字符加数字加特殊字符） |
| 4 | user_sex | VARCHAR（3） | 否 | 否 | 男 | 用户的性别男/女（存汉字） |
| 5 | user_phone | VARCHAR(11) | 否 | 否 | "" | 用户的电话（手机号和座机号）有0开始的就是座机号 |
| 6 | user_avatar | VARCHAR(100) | 否 | 否 | "" | 用户的头像链接（OSS存放） |
| 7 | user_email | VARCHAR(20) | 否 | 否 | "" | 用户邮箱，用来登录和找回账号 |
| 8 | user_status | INT(1) | 否 | 否 | 0 |  0：正常使用
1：非法登录冻结状态；
2：非法操作冻结状态；
3：逻辑删除状态
4：禁用 |
| 9 | create_time | DATE | 否 | 否 | NULL | 用户创建账号时间（默认精确到秒） |
| 9 | update_time | DATE | 否 | 否 | NULL | 用户更新账号时间（默认值为精确到秒） |

序号	字段名称	类型	是否为主键	能否为空	默认值	描述
1	algorithm_id	INT(3)	是	否	自增	按要求给予算法唯一的Id
2	algorithm_name	VARCHAR(30)	否	否	""	算法的名称
3	hot_number	INT(5)	否	否	0	算法的使用量
4	algorithm_describe	VARCHAR(255)	否	否	""	算法的描述
5	algorithm_url	VARCHAR(50)	否	否	""	算法的存放地址(本地的路径)
| 6 | user_id | VARCHAR(8) | 否 | 否 | ID为8为字符前四位为年月，后4位为自增
例：22070001 | 用于辨别不同用户，其取值要按要求取值 |
| 7 | algorithm_state | INT(10) | 否 | 否 | 1 | 0：删除；
1：私有；
2：开源
3：审核中 |
| 8 | algorithm_type | VARCHAR(32) | 否 | 否 | "" | 算法的类型：导入数据预处理/数据建模预处理/建模 |
| 9 | box_test | INT(1) | 否 | 否 | 0 | 是否通过黑盒测试。
0：不通过
1：通过 |
| 10 | create_time | DATE | 否 | 否 | NULL | 创建时间。精确到毫秒保留3位毫秒数 |
| 11 | update_time | DATE | 否 | 否 | NULL | 修改时间。精确到毫秒保留3位毫秒数 |

4.2.2.4涉及物理表
用户表5.2.1,算法表5.2.12

4.2.2.5模块活动图

                                                                        算法上传活动图

4.2.3 数据管理模块
4.2.3.1 模块说明
数据管理模块，把有关物质数据、回收站数据、实验报告数据的管理功能抽取出来的一个模块，主要功能有管理回收站、管理实验报告数据、管理用户物质数据、管理员管理物质类型、还原用户实验报告，调整回收站回收周期等功能

4.2.3.2 涉及具体类
本模块涉及类如图所示：

图 数据管理模块涉及图

4.2.3.3 涉及视图
序号	字段名称	类型	是否为主键	能否为空	默认值	描述
1	report_id	INT(8)	是	否	自增	实验报告唯一的Id
| 2 | user_id | VARCHAR(8) | 是 | 否 | ID为8为字符前四位为年月，后4位为自增
例：22070001 | 用于辨别不同用户 |
| 3 | report_name | VARCHAR(30) | 否 | 否 | "" | 该实验报告的名称（标题） |
| 4 | material_id | INT(8) | 否 | 否 | 自增 | 对应物质的id (对应表3.2.2.1物质数据表) |
| 5 | material_name | VARCHAR(20) | 否 | 否 | “” | 物质名称。与物质数据视图（表9）的物质名称（material_name）对应 |
| 6 | update_time | DATETIME(3) | 否 | 否 | NULL | 创建时间。精确到毫秒保留3位毫秒数 |
| 7 | create_date | DATETIME(3) | 否 | 否 | NULL | 创建时间。精确到毫秒保留3位毫秒数 |
| 8 | report_state | INT | 否 | 否 | 0 | 报告存在状态。
0：正常
1：逻辑删除 |
| 9 | report_describe | VARCHAR(255) | 否 | 否 | "" | 该实验报告的描述和说明 |

表14 实验报告表

序号	字段名称	类型	是否为主键	能否为空	默认值	描述
1	report_id	INT(8)	是	否	自增	实验报告唯一的Id 对应表14实验报告表的实验报告id
| 2 | result_model | varchar(100)

 | 否

 | 否 | "" | 结果模型的存储地址 |
| 3 | analysis_results_diagram

 | varchar(100)

 | 否

 | 否

 | "" | 分析结果图 (
训练集真实值预测值线性拟合图) |
| 4 | trainset_data

 | varchar(100)

 | 否

 | 否

 | ""

 | 包含训练集的原数据和预测结果 |
| 5 | testset_data

 | varchar(100)

 | 否

 | 否

 | "" | 包含测试集的原数据和预测结果 |
| 6 | rc_sqrt

 | DOUBLE

 | 否

 | 否

 | 0.00

 | 训练集决定系数 |
| 7 | rmsec | DOUBLE

 | 否

 | 否

 | 0.00 | 训练集均方根误差

 |
| 8 | maec | DOUBLE

 | 否

 | 否

 | 0.00 | 训练集平均绝对误差

 |
| 9 | rp_sqrt | DOUBLE

 | 否

 | 否

 | 0.00 | 测试集决定系数

 |
| 10 | rmsep | DOUBLE

 | 否

 | 否

 | 0.00 | 测试集均方根误差

 |
| 11 | maep | DOUBLE

 | 否

 | 否

 | 0.00 | 测试集平均误差

 |
| 12 | rpd | DOUBLE

 | 否

 | 否

 | 0.00 | 预测误差 |

表15 实验报告详情表

序号	字段名称	类型	是否为主键	是否能为空	默认值	描述
1	material_id	INT(8)	是	否	自增	按要求给予物质唯一的Id
2	material_name	VARCHAR(21)	否	否	“”	物质名称。由用户自行按格式输入（限定七个汉字）
3	material_type_id	VARCHAR(8)	否	否	空	物质类型对应的id。（对应表8）
4	material_solubility	DOUBLE	否	否	0.00	物质的溶度。为浮点数据类型
5	buffer_solution	VARCHAR(21)	否	否	“”	缓冲溶液。（限定七个汉字）
6	material_remarks	VARCHAR(255)	否	是	“”	备注信息。
7	create_time	DATE	否	否	NULL	创建时间。精确到秒
8	update_time	DATE	否	是	NULL	修改时间。精确到秒
9	material_points	VARCHAR(255)	否	否	“”	点位从数据源文件读取数据，用逗号分隔符进行分割，连接在一起
| 10 | user_id | VARCHAR(8) | 是 | 否 | ID为8为字符前四位为年月，后4位为自增
例：22070001 | 用于辨别不同用户，其取值要按要求取值 |

表 7 物质数据表

| 序号 | 字段名称 | 类型 | 是否为主键 | 是否能为空 | 默认值 | 描述 |
| --- | --- | --- | --- | --- | --- | --- |
| 1 | material_type_id | INT(3) | 是 | 否 | 自增
例：001 | 按要求给予物质类型唯一的Id |
| 2 | material_type_name | VARCHAR(30) | 否 | 否 | “” | 物质类型的名称 |
| 3 | material_type_content | VARCHAR(255) | 否 | 否 | “” | 物质类型描述 |
| 4 | create_time | DATE | 否 | 否 | NULL | 创建时间。精确到秒 |
| 5 | update_time | DATE | 否 | 是 | NULL | 创建时间。精确到秒 |

表8 物质类型表

| 序号 | 字段名称 | 类型 | 是否为主键 | 是否能为空 | 默认值 | 描述 |
| --- | --- | --- | --- | --- | --- | --- |
| 1 | material_specific_id | INT(3) | 是 | 否 | 自增
例：001 | 按要求给予数据详情唯一的Id |
| 2 | algorithm_name | VARCHAR(30) | 否 | 否 | “” | 数据预处理算法名称（限定10个汉字） |
| 3 | algorithm_id | VARCHAR(10) | 否 | 否 | "" | 预处理算法的ID |
| 4 | material_ip | DOUBLE | 否 | 否 | 0.000 | 波形参数分析中得出的电流（精确到小数点后三位） |
| 5 | material_ep | DOUBLE | 否 | 否 | 0.000 | 波形参数分析中得出的电位值（精确到小数点后三位） |
| 6 | materil_state | INT | 否 | 否 | 0 | 0：原始数据；
1：数据预处理状态；
2：波形参数分析； |
| 7 | left_point | VARCHAR(20) | 否 | 否 | "" | 左点位 |
| 8 | right_point | VARCHAR(20) | 否 | 否 | "" | 右点位 |
| 9 | high_point | VARCHAR(20) | 否 | 否 | "" | 最高点位 |

表9 数据详情表

4.2.3.4 涉及物理表
涉及物质表在本文档第五部分的5.2.7~5.2.11表

4.2.3.5 模块活动图
#### 


图 管理物质数据信息说明图
#### 


图 管理回收站说明图
#### 


图 管理实验报告说明图
#### 


图 管理物质类型说明图

4.2.4 日志管理模块
4.2.4.1 模块说明
日志管理模块，主要功能有增加操作日志、查询所有日志、查询日志、导出日志等。

4.2.4.2 涉及具体类
本模块涉及类如图所示：

图 用户管理模块涉及图

4.2.4.3 涉及视图
| 序号 | 字段名称 | 类型 | 是否为主键 | 是否能为空 | 默认值 | 描述 |
| --- | --- | --- | --- | --- | --- | --- |
| 1 | user_id | VARCHAR(8) | 是 | 否 | ID为8为字符前四位为年月，后4位为自增
例：22070001 | 用于辨别不同用户，其取值要按要求取值 |
| 2 | user_name | VARCHAR(12) | 否 | 否 | “” | 用户名，由用户自己提供（只能有汉字和英语字符） |
| 3 | user_password  | VARCHAR(15) | 否 | 否 | “Aa1234567?” | MD的用户密码。（大小字符加数字加特殊字符） |
| 4 | user_sex | VARCHAR（3） | 否 | 否 | 男 | 用户的性别男/女（存汉字） |
| 5 | user_phone | VARCHAR(11) | 否 | 否 | "" | 用户的电话（手机号和座机号）有0开始的就是座机号 |
| 6 | user_avatar | VARCHAR(100) | 否 | 否 | "" | 用户的头像链接（OSS存放） |
| 7 | user_email | VARCHAR(20) | 否 | 否 | "" | 用户邮箱，用来登录和找回账号 |
| 8 | user_status | INT(1) | 否 | 否 | 0 |  0：正常使用
1：非法登录冻结状态；
2：非法操作冻结状态；
3：逻辑删除状态
4：禁用 |
| 9 | create_time | DATE | 否 | 否 | NULL | 用户创建账号时间（默认精确到秒） |
| 9 | update_time | DATE | 否 | 否 | NULL | 用户更新账号时间（默认值为精确到秒） |

                                                                                  表 用户表
序号	字段名称	类型	是否为主键	是否能为空	默认值	描述
1	log_id	INT(6)	是	否	自增	日志唯一ID
2	log_url	VARCHAR(50)	否	否	“”	日志保存的url
| 3 | log_state  | INT(1) | 否 | 否 |  “” | 0：正常
1：异常 |
| 4 |  log_content | VCARHAR(50) |  否 |  否 |  “” | 日志内容描述 |
| 5 | hander_id | VARCHAR(10) | 否 | 是 | “” | 员工id的编号 |
| 6 | hander_ip | VARCHAR(32) | 否 | 否 | "" | 员工操作的ip地址 |
| 7 | create_time | DATE | 否 | 否 | NULL | 创建时间。精确到秒 |
| 8 | update_time | DATE | 否 | 否 | NULL | 修改时间。精确到秒 |

                                                                                        表 日志表
4.2.4.4 涉及物理表
涉及物质表在本文档第五部分的5.2.1和5.2.14表

4.2.4.5 模块活动图

                                                                 导出日志前后端活动图

                                                              插入日志前后端活动图

                                                                                      管理员查询所有日志前后端活动图

                                                                                      管理员根据用户名查询日志前后端活动图

4.2.5 备份管理模块
4.2.5.1 模块说明
系统备份管理模块，将所有有关备份的管理功能全部抽取成一个管理模块，该模块主要功能包括系统自动备份、手动备份、用户还原功能、设置自动备份周期等

4.2.5.2 涉及具体类
本模块涉及类如图所示：

图 备份管理模块涉及图

4.2.5.3 涉及视图
序号	字段名称	类型	是否为主键	能否为空	默认值	描述
1	backups_id	INT(6)	是	否	自增	备份唯一ID
2	user_id	VARCHAR(8)	否	否	""	备份数据目标用户ID
3	backups_time	DATE	否	否	NULL	修改时间。精确到天
4	user_name	VARCHAR(12)	否	否	0	存储当前备份记录的用户名
5	backups_version	INT(1)	否	否	0	备份版本号(系统只存5个版本)
6	backups_remark	VARCHAR(255)	否	否	0	备份当前版本的描述内容
表 备份视图表 

4.2.5.4 涉及物理表
涉及用户表和部分表分别在本文档第五部分 5.2中的5.2.1 用户表和3.2.13部分数据表

4.2.5.5 模块活动图

图 数据还原说明图

图 更改备份周期说明图

图 系统备份说明图

4.2.6 数据分析模块
4.2.6.1 模块说明
数据分析模块，把有关系统的管理功能抽取出来成为一个模块，主要功能有实验数据导入、数据预处理、数据波形参数分析、数据建模等。

4.2.6.2 涉及具体类
本模块涉及类如图所示：

图 用户管理模块涉及图

4.2.6.3 涉及视图
序号	字段名称	类型	是否为主键	是否能为空	默认值	描述
1	material_id	INT(8)	是	否	自增	按要求给予物质唯一的Id
2	material_name	VARCHAR(21)	否	否	“”	物质名称。由用户自行按格式输入（限定七个汉字）
3	material_type_id	VARCHAR(8)	否	否	空	物质类型对应的id。（对应表8）
4	material_solubility	DOUBLE	否	否	0.00	物质的溶度。为浮点数据类型
5	buffer_solution	VARCHAR(21)	否	否	“”	缓冲溶液。（限定七个汉字）
6	material_remarks	VARCHAR(255)	否	是	“”	备注信息。
7	create_time	DATE	否	否	NULL	创建时间。精确到秒
8	update_time	DATE	否	是	NULL	修改时间。精确到秒
9	material_points	VARCHAR(255)	否	否	“”	点位从数据源文件读取数据，用逗号分隔符进行分割，连接在一起
| 10 | user_id | VARCHAR(8) | 是 | 否 | ID为8为字符前四位为年月，后4位为自增
例：22070001 | 用于辨别不同用户，其取值要按要求取值 |

表  物质数据表

| 序号 | 字段名称 | 类型 | 是否为主键 | 是否能为空 | 默认值 | 描述 |
| --- | --- | --- | --- | --- | --- | --- |
| 1 | material_type_id | INT(3) | 是 | 否 | 自增
例：001 | 按要求给予物质类型唯一的Id |
| 2 | material_type_name | VARCHAR(30) | 否 | 否 | “” | 物质类型的名称 |
| 3 | material_type_content | VARCHAR(255) | 否 | 否 | “” | 物质类型描述 |
| 4 | create_time | DATE | 否 | 否 | NULL | 创建时间。精确到秒 |
| 5 | update_time | DATE | 否 | 是 | NULL | 创建时间。精确到秒 |

表 物质类型表

| 序号 | 字段名称 | 类型 | 是否为主键 | 是否能为空 | 默认值 | 描述 | 约束 |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 1 | material_specific_id | INT(3) | 是 | 否 | 自增
例：001 | 按要求给予数据详情唯一的Id | 无 |
| 2 | algorithm_name | VARCHAR(30) | 否 | 否 | “” | 数据预处理算法名称（限定10个汉字） | 无 |
| 3 | algorithm_id | VARCHAR(10) | 否 | 否 | "" | 预处理算法的ID | 无 |
| 4 | material_ip | DOUBLE | 否 | 否 | 0.000 | 波形参数分析中得出的电流（精确到小数点后三位） | 无 |
| 5 | material_ep | DOUBLE | 否 | 否 | 0.000 | 波形参数分析中得出的电位值（精确到小数点后三位） | 无 |
| 6 | materil_state | INT | 否 | 否 | 0 | 0：原始数据；
1：数据预处理状态；
2：波形参数分析； | 无 |
| 7 | left_point | VARCHAR(20) | 否 | 否 | "" | 左点位 | 无 |
| 8 | right_point | VARCHAR(20) | 否 | 否 | "" | 右点位 | 无 |
| 9 | high_point | VARCHAR(20) | 否 | 否 | "" | 最高点位 | 无 |
| 10 | material_id | INT(8) | 是 | 否 | 自增 | 对应物质表中的Id | 外键约束：物质表中对应的id删除的话，本条数据删除 |

表 数据详情表

4.2.6.4 涉及物理表
涉及物质表在本文档第五部分的5.2.7物质数据表、5.2.8物质类型表、5.2.9数据详情表、5.2.10实验报告表、5.2.12算法表

4.2.6.5 模块活动图

图 导入数据说明图

图 数据预处理说明图

图 数据波形参数分析说明图

图 数据建模说明图

4.2.7 查询管理模块
4.2.7.1模块说明
系统查询管理模块，将所有有关查询的功能全部抽取成一个查询模块，该模块将前端界面的查询功能和后端的查询数据功能全部抽取出来 作为一个查询模块。

4.2.7.2 模块活动图

图 查询管理说明图

五、数据库设计
5.1数据ER图
5.1.1实体
用户

角色

权限

申请订单

物质

物质类型

详情数据

实验数据

算法

日志

备份

5.1.2联系
用户拥有角色（n对n）

角色具有权限（n对n）

用户申请订单（1对n）

用户产生日志（1对n）

用户生成备份（1对n）

用户添加物质（1对n）

物质建模实验数据（n对1）

物质查看详细数据（1对1）

物质属于物质类型（1对n）

物质使用算法（n对1）

5.1.3 E-R图


5.2数据表
5.2.1用户表
| 序号 | 字段名称 | 类型 | 是否为主键 | 是否能为空 | 默认值 | 描述 | 约束 |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 1 | user_id | VARCHAR(8) | 是 | 否 | ID为8为字符前四位为年月，后4位为自增
例：22070001 | 用于辨别不同用户，其取值要按要求取值 | 主键约束，不能为空 |
| 2 | user_name | VARCHAR(12) | 否 | 否 | “” | 用户名，由用户自己提供（只能有汉字和英语字符） | 无 |
| 3 | user_password  | VARCHAR(15) | 否 | 否 | “Aa1234567?” | MD的用户密码。（大小字符加数字加特殊字符） | 无 |
| 4 | user_sex | VARCHAR（3） | 否 | 否 | 男 | 用户的性别男/女（存汉字） | 无 |
| 5 | user_phone | VARCHAR(11) | 否 | 否 | "" | 用户的电话（手机号和座机号）有0开始的就是座机号 | 无 |
| 6 | user_avatar | VARCHAR(100) | 否 | 否 | "" | 用户的头像链接（OSS存放） | 无 |
| 7 | user_email | VARCHAR(20) | 否 | 否 | "" | 用户邮箱，用来登录和找回账号 | 无 |
| 8 | user_status | INT(1) | 否 | 否 | 0 |  0：正常使用
1：非法登录冻结状态；
2：非法操作冻结状态；
3：逻辑删除状态
4：禁用 | 无 |
| 9 | create_time | DATE | 否 | 否 | NULL | 用户创建账号时间（默认精确到秒） | 无 |
| 10 | update_time | DATE | 否 | 否 | NULL | 用户更新账号时间（默认值为精确到秒） | 无 |

5.2.2权限表
| 序号 | 字段名称 | 类型 | 是否为主键 | 是否能为空 | 默认值 | 描述 | 约束 |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 1 | permissions_id | INT(2) | 是 | 否 | 自增
例：01 | 按要求给予权限唯一的Id | 主键约束 |
| 2 | permissions_name | VARCHAR(18) | 否 | 否 | “数据分析模块” | 角色所能够使用的权限模块名称 | 无 |
| 3 | permissions_url | VARCHAR(32) | 否 | 否 | “\index” | 本权限拦截器放行路径 | 无 |

5.2.3角色表
| 序号 | 字段名称 | 类型 | 是否为主键 | 是否为空 | 默认值 | 描述 | 约束 |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 1 | role_id | INT(2) | 是 | 否 | 自增
例：01 | 按要求给予角色唯一的Id | 主键约束 |
| 2 | role_name | VARCHAR(10) | 否 | 否 | “” | 角色名称（“研究员”，“管理员”，“超级管理员”） | 无 |
| 3 | role_content | VARCHAR(255) | 否 | 否 | “” | 角色具体描述（主要描述角色含有的模块权限且大致的操作权限） | 无 |

5.2.4角色、权限表
序号	字段名称	类型	是否为主键	是否能为空	默认值	描述	约束
1	role_id	VARCHAR(10)	否	否	“”	角色ID	和角色表id绑定，当角色表数据删除时改数据也会删除
2	permissions _id	VARCHAR(10)	否	否	“”	权限ID	和权限表id绑定，当权限表数据删除时改数据也会删除
5.2.5角色、用户表
序号	字段名称	类型	是否为主键	是否能为空	默认值	描述	约束
1	role_id	VARCHAR(10)	否	否	“”	角色ID	和角色表id绑定，当角色表数据删除时改数据也会删除
2	user_id	VARCHAR(10)	否	否	“”	用户ID	和用户表id绑定，当用户表数据删除时改数据也会删除
5.2.6申请订单表
| 序号 | 字段名称 | 类型 | 是否为主键 | 是否能为空 | 默认值 | 描述 | 约束 |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 1 | request_id | INT(2) | 是 | 否 | 自增
例：01 | 按要求给予申请唯一的Id | 无 |
| 2 | request_content | VARCHAR(255) | 否 | 否 | "" | 申请理由 | 无 |
| 3 | user_id | VARCHAR(8) | 否 | 否 | "" | 用户ID | 和用户表id绑定，当用户表数据删除时改数据也会删除 |
| 4 | request_type | INT(2) | 否 | 否 | "" | 请求类型
0：解冻
1：上传算法
2：上传物质类型
3：数据还原 | 无 |
| 5 | request_handler_id | VARCHAR(8) | 否 | 否 | "" | 处理该申请的管理员用户ID | 无 |
| 5 | create_time | DATE | 否 | 否 | NULL | 申请创建账号时间（默认值为精确到秒） | 无 |
| 6 | update_time | DATE | 否 | 否 | NULL | 申请更新账号时间（默认值为精确到秒） | 无 |

5.2.7物质数据表
序号	字段名称	类型	是否为主键	是否能为空	默认值	描述	约束
1	material_id	INT(8)	是	否	自增	按要求给予物质唯一的Id	主键约束：删除本条数据时，会把关联的数据删除
2	material_name	VARCHAR(21)	否	否	“”	物质名称。由用户自行按格式输入（限定七个汉字）	无
3	material_type_id	INT(3)	否	否	空	物质类型对应的id。（对应表8）	外键约束。当物质类型表中的对应数据删除时，本条数据也删除
4	material_solubility	DOUBLE	否	否	0.00	物质的溶度。为浮点数据类型	无
5	buffer_solution	VARCHAR(21)	否	否	“”	缓冲溶液。（限定七个汉字）	无
6	material_remarks	VARCHAR(255)	否	是	“”	备注信息。	无
7	create_time	DATE	否	否	NULL	创建时间。精确到秒	无
8	update_time	DATE	否	是	NULL	修改时间。精确到秒	无
| 9 | user_id | VARCHAR(8) | 是 | 否 | ID为8为字符前四位为年月，后4位为自增
例：22070001 | 用于辨别不同用户，其取值要按要求取值 | 外键约束。当用户表中的对应数据删除时，本条数据也删除 |
| 10 | state | INT | 是 | 否 | 0 | 0:正常；1：删除状态 |  |

5.2.8物质类型表
| 序号 | 字段名称 | 类型 | 是否为主键 | 是否能为空 | 默认值 | 描述 | 约束 |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 1 | material_type_id | INT(3) | 是 | 否 | 自增
例：001 | 按要求给予物质类型唯一的Id | 主键约束 |
| 2 | material_type_name | VARCHAR(30) | 否 | 否 | “” | 物质类型的名称 | 无 |
| 3 | material_type_content | VARCHAR(255) | 否 | 否 | “” | 物质类型描述 | 无 |
| 4 | create_time | DATE | 否 | 否 | NULL | 创建时间。精确到秒 | 无 |
| 5 | update_time | DATE | 否 | 是 | NULL | 修改时间。精确到秒 | 无 |

5.2.9数据详情表
| 序号 | 字段名称 | 类型 | 是否为主键 | 是否能为空 | 默认值 | 描述 | 约束 |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 1 | material_specific_id | INT(3) | 是 | 否 | 自增
例：001 | 按要求给予数据详情唯一的Id | 主键约束 |
| 2 | algorithm_name | VARCHAR(30) | 否 | 否 | “” | 数据预处理算法名称（限定10个汉字） | 无 |
| 3 | algorithm_id | VARCHAR(10) | 否 | 否 | "" | 预处理算法的ID | 外键约束。当算法表的对应数据删除时，本条数据也删除 |
| 4 | material_ip | DOUBLE | 否 | 否 | 0.000 | 波形参数分析中得出的电流（精确到小数点后三位） | 无 |
| 5 | material_ep | DOUBLE | 否 | 否 | 0.000 | 波形参数分析中得出的电位值（精确到小数点后三位） | 无 |
| 6 | materil_state | INT | 否 | 否 | 0 | 0：原始数据；
1：数据预处理状态；
2：波形参数分析；
3：删除状态； | 无 |
| 7 | left_point | VARCHAR(20) | 否 | 否 | "" | 左点位 | 无 |
| 8 | right_point | VARCHAR(20) | 否 | 否 | "" | 右点位 | 无 |
| 9 | high_point | VARCHAR(20) | 否 | 否 | "" | 最高点位 | 无 |
| 10 | material_id | INT(8) | 是 | 否 | 自增 | 对应物质表的Id | 外键约束：物质表中对应的id删除的话，本条数据删除 |
| 11 | material_points | VARCHAR(255) | 否 | 否 | “” | 点位从数据源文件读取数据，左点位与右点位用逗号进行分割，每个数据之间用分号进行连接 | 无 |

5.2.10实验报告表
序号	字段名称	类型	是否为主键	能否为空	默认值	描述	约束
1	report_id	INT(8)	是	否	自增	实验报告唯一的Id	主键约束
| 2 | user_id | VARCHAR(8) | 否 | 否 | ID为8为字符前四位为年月，后4位为自增
例：22070001 | 用于辨别不同用户 | 外键约束，和用户表id相关，用户表数据删除，该数据也会删除 |
| 3 | report_name | VARCHAR(30) | 否 | 否 | "" | 该实验报告的名称（标题） | 无 |
| 4 | material_id | INT(8) | 否 | 否 | 自增 | 对应物质的id (对应表3.2.2.1物质数据表) | 外键约束，和物质id相关，物质表数据删除，该数据也会删除 |
| 5 | material_name | VARCHAR(20) | 否 | 否 | “” | 物质名称。与物质数据视图（表9）的物质名称（material_name）对应 | 外键约束，和物质表名称相关，物质数据删除，该数据也会删除 |
| 6 | update_time | DATETIME(3) | 否 | 否 | NULL | 创建时间。精确到毫秒保留3位毫秒数 | 无 |
| 7 | create_time | DATETIME(3) | 否 | 否 | NULL | 创建时间。精确到毫秒保留3位毫秒数 | 无 |
| 8 | report_describe | VARCHAR(255) | 否 | 否 | "" | 该实验报告的描述和说明 | 无 |
| 9 | report_state | INT | 否 | 否 | 0 | 报告存在状态。
0：正常
1：逻辑删除 | 无 |

5.2.11实验报告详情表
序号	字段名称	类型	是否为主键	能否为空	默认值	描述	约束
1	report_id	INT(8)	是	否	自增	实验报告唯一的Id 对应表14实验报告表的实验报告id	主键约束
| 2 | result_model | varchar(100)

 | 否 | 是 | "" | 结果模型的存储地址 | 无 |
| 3 | analysis_results_diagram

 | varchar(100)

 | 否 | 是 | "" | 分析结果图 (
训练集真实值预测值线性拟合图) | 无 |
| 4 | trainset_data

 | varchar(100)

 | 否 | 是 | ""

 | 包含训练集的原数据和预测结果 | 无 |
| 5 | testset_data

 | varchar(100)

 | 否 | 是 | "" | 包含测试集的原数据和预测结果 | 无 |
| 6 | rc_sqrt

 | DOUBLE

 | 否 | 是 | 0.00

 | 训练集决定系数 | 无 |
| 7 | rmsec | DOUBLE

 | 否 | 是 | 0.00 | 训练集均方根误差

 | 无 |
| 8 | maec | DOUBLE

 | 否 | 是 | 0.00 | 训练集平均绝对误差

 | 无 |
| 9 | rp_sqrt | DOUBLE

 | 否 | 是 | 0.00 | 测试集决定系数

 | 无 |
| 10 | rmsep | DOUBLE

 | 否 | 是 | 0.00 | 测试集均方根误差

 | 无 |
| 11 | maep | DOUBLE

 | 否 | 是 | 0.00 | 测试集平均误差

 | 无 |
| 12 | rpd | DOUBLE

 | 否 | 是 | 0.00 | 预测误差 | 无 |

5.2.12算法表
序号	字段名称	类型	是否为主键	能否为空	默认值	描述	约束
1	algorithm_id	INT(3)	是	否	自增	按要求给予算法唯一的Id	主键约束
2	algorithm_name	VARCHAR(30)	否	否	""	算法的名称	无
3	hot_number	INT(5)	否	否	0	算法的使用量	无
4	algorithm_describe	VARCHAR(255)	否	否	""	算法的描述	无
5	algorithm_url	VARCHAR(50)	否	否	""	算法的存放地址(本地的路径)	无
| 6 | user_id | VARCHAR(8) | 否 | 否 | ID为8为字符前四位为年月，后4位为自增
例：22070001 | 用于辨别不同用户，其取值要按要求取值 | 外键约束，和用户表id相关，用户表数据删除，该数据也会删除 |
| 7 | algorithm_state | INT(10) | 否 | 否 | 1 | 0：删除；
1：私有；
2：开源
3：审核中 | 无 |
| 8 | algorithm_type | VARCHAR(32) | 否 | 否 | "" | 算法的类型：导入数据预处理/数据建模预处理/建模 | 无 |
| 9 | box_test | INT(1) | 否 | 否 | 0 | 是否通过黑盒测试。
0：不通过
1：通过 | 无 |
| 10 | create_time | DATE | 否 | 否 | NULL | 创建时间。精确到毫秒保留3位毫秒数 | 无 |
| 11 | update_time | DATE | 否 | 否 | NULL | 修改时间。精确到毫秒保留3位毫秒数 | 无 |

5.2.13 备份数据表
序号	字段名称	类型	是否为主键	能否为空	默认值	描述	约束
1	backups_id	INT(6)	是	否	自增	备份唯一ID	主键约束
2	user_id	VARCHAR(8)	否	否	""	备份数据目标用户ID	外键约束，和用户表id相关，用户表数据删除，该数据也会删除
3	backups_time	DATE	否	否	NULL	备份时间，如果在提同一天备份的数据当前版本的备份自动覆盖当前之内的版本作为一个备份版本。精确到天	无
4	backups_version	INT(1)	否	否	0	备份版本号(系统只存5个版本)	无
5	backups_remark	VARCHAR(255)	否	否	0	备份当前版本的描述内容	无
5.2.14日志表
序号	字段名称	类型	是否为主键	是否能为空	默认值	描述	约束
1	log_id	INT(6)	是	否	自增	日志唯一ID	主键约束
2	log_url	VARCHAR(50)	否	否	“”	日志保存的url	无
| 3 | log_state  | INT(1) | 否 | 否 |  “” | 0：正常
1：异常 | 无 |
| 4 |  log_content | VCARHAR(50) |  否 |  否 |  “” | 日志内容描述 | 无 |
| 5 | hander_id | VARCHAR(10) | 否 | 是 | “” | 员工id的编号 | 外键约束，和用户表id相关，用户表数据删除，该数据也会删除 |
| 6 | hander_ip | VARCHAR(32) | 否 | 否 | "" | 员工操作的ip地址 | 无 |
| 7 | create_time | DATE | 否 | 否 | NULL | 创建时间。精确到秒 | 无 |
| 8 | update_time | DATE | 否 | 否 | NULL | 修改时间。精确到秒 | 无 |

六、操作界面