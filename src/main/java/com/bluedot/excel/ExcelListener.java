package com.bluedot.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.List;
import java.util.Map;

/**
 * 客户信息监听器
 * @author QiYu
 */
public class ExcelListener<T> extends AnalysisEventListener<T> {


    private List<T> list;

    public ExcelListener(List<T> list) {
        this.list = list;
    }

    public List<T> getList() {
        return this.list;
    }

    //一行一行读取excel内容
    @Override
    public void invoke(T data, AnalysisContext analysisContext) {
        list.add(data);
    }

    //读取表头内容
    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        System.out.println("表头：" + headMap);
    }

    //读取完成之后
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
    }
}

