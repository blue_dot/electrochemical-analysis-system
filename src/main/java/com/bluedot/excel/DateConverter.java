package com.bluedot.excel;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;


/**
 * 日期类解析转换器
 * @author QiYu
 */
public class DateConverter implements Converter<Object> {
    @Override
    public Class supportJavaTypeKey() {
        return Object.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public Object convertToJavaData(CellData cellData, ExcelContentProperty contentProperty,
                                    GlobalConfiguration globalConfiguration) throws ParseException {

        return new Date(new SimpleDateFormat(contentProperty.getDateTimeFormatProperty().getFormat()).parse(cellData.getStringValue()).getTime());

    }

    @Override
    public CellData<String> convertToExcelData(Object date, ExcelContentProperty excelContentProperty, GlobalConfiguration globalConfiguration) {
        return new CellData(new SimpleDateFormat(excelContentProperty.getDateTimeFormatProperty().getFormat()
        ).format(date));
    }

}

