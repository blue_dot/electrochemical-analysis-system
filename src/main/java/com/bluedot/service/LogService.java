package com.bluedot.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bluedot.controller.DistributeClass;
import com.bluedot.pojo.pack.PreMappedStatement;
import com.bluedot.pojo.pack.ResultEntity;
import com.bluedot.pojo.vo.LogVo;
import com.bluedot.pojo.pack.FileResponse;
import com.bluedot.pojo.vo.UserVO;
import com.bluedot.utils.ExcelUtils;


import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *<p>描述:日志业务类 <p>
 *@author QiYu
 *@CreateDate 2022/8/18 10:36
 */

public class LogService extends DistributeClass {


    private UtilTool util;
    private LogVo logVo;
    private ArrayList<LogVo> list=new ArrayList<>();


    public LogService(PreMappedStatement<String> preMappedStatement) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        super(preMappedStatement);

        String[] split = preMappedStatement.getMethodType().split("/");
        switch (split[split.length - 1]) {
            case "query":
                getResponseQueue().add(ResultEntity.successWithData(query()));
                break;
            case "export":
                getResponseQueue().add(ResultEntity.successWithData(exportLogs()));
                break;
            default:
                throw new RuntimeException("url未匹配");

        }

    }


    @Override
    protected boolean insert() {
        return super.insert();
    }

    @Override
    protected List query() {
        util = new UtilTool();
        logVo = new LogVo();
        try {
            if (((String) preMappedStatement.getData()).length() > 0)
                logVo = (LogVo) util.JSONtoObject((String) preMappedStatement.getData(), LogVo.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        String jsonString = (String) preMappedStatement.getData();

        if (jsonString.contains("logIds")) {
            JSONObject jsonObject = JSONObject.parseObject(preMappedStatement.getData().toString());
            JSONArray jsonArray = jsonObject.getJSONArray("logIds");
            List<Integer> list = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                list.add((Integer) jsonArray.get(i));
            }
           // List<Integer> list = handlerString(jsonString);
            List<Object> query = util.query(new PreMappedStatement(UtilTool.QUERY, new LogVo()));
            if (query == null) return null;
            List<LogVo> collect = query.stream().map(item -> {
                return (LogVo) item;
            }).collect(Collectors.toList());

            for (Integer integer : list) {
                 LogVo logVo = collect.stream().filter(item -> item.getLogId().equals(integer)).findFirst().orElse(null);
                 if (logVo == null) continue;
                 this.list.add(logVo);
            }
        }
        return util.query(new PreMappedStatement<>(UtilTool.QUERY, logVo));
    }

    @Override
    protected boolean update() {
        return super.update();
    }

    @Override
    protected boolean delete() {
        return super.delete();
    }

    private FileResponse exportLogs() {
        query();
        try {
            return new FileResponse("日志信息.xlsx", ExcelUtils.logToExcel(list, new ByteArrayOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

/**
 *<p>描述: 对于前端传来的logIds数组进行处理<p>
 *@param
 *@return
 *@author QiYu
 *@CreateDate 2022/8/17 22:20
 *@update [序号][日期YYYY-MM-DD][王震宇][变更描述]
 */
    public List<Integer> handlerString(String jsonString) {
        ArrayList<Integer> integers = new ArrayList<>();
        String s = jsonString.replaceAll("\\s*|\r|\n|\t", "")
                .replaceAll("\"", "")
                .replaceAll(",", "")
                .split("\\[")[1]
                .split("]")[0];
        for (char c : s.toCharArray()) {
            integers.add(c-'0');
        }
        return integers;
    }


}
