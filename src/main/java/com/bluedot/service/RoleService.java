package com.bluedot.service;

import com.bluedot.controller.DistributeClass;
import com.bluedot.pojo.pack.PreMappedStatement;
import com.bluedot.pojo.pack.ResultEntity;
import com.bluedot.pojo.po.RoleAndPermissions;
import com.bluedot.pojo.po.RoleDTO;
import com.bluedot.pojo.po.RolePO;
import com.bluedot.pojo.vo.PermissionsVO;
import com.bluedot.pojo.vo.RoleVO;

import javax.management.relation.RelationNotification;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * 角色管理类
 * @author chenzou'quan
 */
public class RoleService extends DistributeClass {
    private UtilTool util;

    public RoleService(PreMappedStatement param) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        super(param);
        this.util = new UtilTool();
        String[] split = preMappedStatement.getMethodType().split("/");
        switch (split[split.length - 1]){
            default:
                break;
        }
    }

    /**
     * 创建角色方法
     * @return
     */
    @Override
    protected boolean insert() {
        if (util == null){
            util = new UtilTool();
        }
        RoleVO data = (RoleVO) preMappedStatement.getData();
        List<Integer> permissions = data.getPermissions();
        data.setPermissions(null);
        // 1. 查找roleId
        List<Object> query = util.query(new PreMappedStatement<>(UtilTool.QUERY, new RolePO()));
        Integer id = 0;
        for (Object tmp: query) {
            Integer roleId = ((RolePO) tmp).getRoleId();
            if (id <= roleId){
                id = roleId;
            }
        }
        id++;
        data.setRoleId(id);
        // 2. 先插入默认的角色数据
        boolean insert = util.insert(new PreMappedStatement<>(UtilTool.INSERT, new RolePO(data.getRoleId(), data.getRoleName(), data.getRoleContent())));
        if (!insert){
            throw new RuntimeException("插入角色默认信息错误");
        }
        // 3. 插入角色权限信息
        for (Integer pid: permissions) {
            boolean insertPid = util.insert(new PreMappedStatement<>(UtilTool.INSERT, new RoleAndPermissions(id, pid)));
            if (!insertPid){
                throw new RuntimeException("插入角色权限信息错误");
            }
        }
        return true;
    }

    /**
     * 查询角色接口
     * @return
     */
    @Override
    protected List query() {
        if (util == null){
            util = new UtilTool();
        }
        RoleVO data = (RoleVO) preMappedStatement.getData();
        // 1.先查po
        List query = util.query(new PreMappedStatement<>(UtilTool.QUERY,new RolePO(data.getRoleId(), data.getRoleName(), data.getRoleContent())));
        if (query == null){
            getResponseQueue().add(ResultEntity.falseWithoutData("查询失败"));
        }
        List<RoleVO> roleVOS = new ArrayList<>();
        for (Object tmp: query) {
            RolePO rolePO = (RolePO) tmp;
            // 1.1 设置一个VO类
            RoleVO roleVO = new RoleVO();
            // 1.2 初始化vo
            roleVO.setRoleId(rolePO.getRoleId());
            roleVO.setRoleContent(rolePO.getRoleContent());
            roleVO.setRoleName(rolePO.getRoleName());
            // 2.再查权限
            List permissionsVO = util.query(new PreMappedStatement<>(UtilTool.QUERY, new PermissionsVO(rolePO.getRoleId())));
            if (permissionsVO != null){
                // 转换为permissionsVO队列
                List<String> permissionsName = new ArrayList<>();
                for (Object tmp_: permissionsVO) {
                    PermissionsVO permissions = (PermissionsVO) tmp_;
                    permissionsName.add(String.valueOf(permissions.getPermissionsId()));
                }
                roleVO.setPermissions(permissionsName);
            }
            // 3.放入队列中
            roleVOS.add(roleVO);
        }
        getResponseQueue().add(ResultEntity.successWithData(roleVOS));
        return roleVOS;
    }

    /**
     * 修改角色接口
     * @return
     */
    @Override
    protected boolean update() {
        if (util == null){
            util = new UtilTool();
        }
        // 1.获得已经定位的数据
        RoleVO data = (RoleVO) preMappedStatement.getData();
        // 2.获得原数据id
        Integer roleId = data.getRoleId();
        // 3.获得目前角色的权限信息
        List<Integer> permissions = data.getPermissions();
        // 4.获得操作对象
        RolePO rolePO = new RolePO(data.getRoleId(), data.getRoleName(), data.getRoleContent());
        // 5. 修改基本数据
        boolean update = util.update(new PreMappedStatement<>(UtilTool.UPDATE, rolePO));
        if (!update){
            throw new RuntimeException("修改基本信息错误");
        }
        // 6. 修改权限
        // 6.1 先删除原有权限
        try {
            boolean delete = util.delete(new PreMappedStatement<>(UtilTool.DELETE, new RoleAndPermissions(data.getRoleId(),null)));
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("角色权限变更失败");
        }
        // 6.2 查入数据库
        for ( Integer pid:permissions) {
            boolean insert = util.insert(new PreMappedStatement<>(UtilTool.INSERT, new RoleAndPermissions(roleId,pid)));
            if (!insert){
                throw new RuntimeException("权限信息插入错误");
            }
        }
        return true;
    }

    /**
     * 删除角色接口
     * @return
     */
    @Override
    protected boolean delete() {
        if (util == null){
            util = new UtilTool();
        }
        List<RoleVO> data = (List<RoleVO>) preMappedStatement.getData();
        for (RoleVO tmp: data) {
            RolePO rolePO = new RolePO();
            rolePO.setRoleId(tmp.getRoleId());
            boolean delete = util.delete(new PreMappedStatement<>(UtilTool.DELETE,rolePO));
            if (!delete){
                throw new RuntimeException("删除失败");
            }
        }
        return true;
    }
}
