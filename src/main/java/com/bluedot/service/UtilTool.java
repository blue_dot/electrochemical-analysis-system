package com.bluedot.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.bluedot.dao.Data;
import com.bluedot.pojo.pack.PreMappedStatement;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 工具类
 *
 * @author chenzou'quan
 */
public class UtilTool {
    public static final String QUERY = "query";
    public static final String DELETE = "delete";
    public static final String INSERT = "insert";
    public static final String UPDATE = "update";
    private Data data;

    public UtilTool() {
        data = new Data();
    }

    /**
     * json转换为Object对象方法
     *
     * @param json
     * @param object 对象class类型
     * @return 对象
     */
    public Object JSONtoObject(String json, Class<?> object) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        // 1. 格式化字符串为map
        Map parse = (Map) JSON.parse(json);
        // 2. 获取属性数组
        Field[] declaredFields = object.getDeclaredFields();
        // 3.创建对应对象
        Object instance = object.getDeclaredConstructor().newInstance();
        // 4. 设置对应参数
        for (Field field : declaredFields) {
            String fieldName = field.getName();
            String typeName = field.getType().getSimpleName();
            String setterName = getSetterName(fieldName);
            if (parse.get(fieldName) != null) {
                Method setter;
                try {
                    setter = object.getMethod(setterName, field.getType());
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
                switch (typeName) {
                    case "int":
                    case "Integer":
                    case "boolean":
                    case "Boolean":
                    case "String":
                    case "Date":
                    case "Double":
                    case "double":
                    case "Float":
                    case "float":
                    case "Short":
                    case "short":
                    case "Long":
                    case "long":
                    case "Byte":
                    case "byte":
                        setter.invoke(instance, parse.get(fieldName));
                        break;
                    case "List":
                        JSONArray array = (JSONArray) parse.get(fieldName);
                        setter.invoke(instance, array);
                        break;
                }
            }
        }
        return instance;
    }

    /**
     * 获得setter方法名称
     *
     * @param fieldName
     * @return
     */
    private String getSetterName(String fieldName) {
        return "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
    }

    /**
     * 查询方法
     * @param preMappedStatement
     * @return
     */
    public List<Object> query(PreMappedStatement preMappedStatement) {
        List list = null;
        if (data.dataAdd(preMappedStatement).getClass() != Integer.class){
            list = (List) data.dataAdd(preMappedStatement);
        }
        if (list == null || list.size() <= 0) {
//            throw new RuntimeException("未找到对应数据");
            return null;
        }
        return list;
    }


    /**
     * 备份数据库查询
     * @param preMappedStatement
     * @param connection
     * @return
     */
    public List<Object> queryForBackups(PreMappedStatement preMappedStatement, Connection connection) {
        List list = (List) data.dataAddForBackups(preMappedStatement, connection);
        if (list == null || list.size() <= 0) {
//            throw new RuntimeException("未找到对应数据");
            return null;
        }
        return list;
    }

    /**
     * 删除方法
     *
     * @param preMappedStatement
     * @return
     */
    public boolean delete(PreMappedStatement preMappedStatement) {
        int flag = (int) data.dataAdd(preMappedStatement);
        return flag == 1;
    }

    /**
     * 增加方法
     *
     * @param preMappedStatement
     * @return
     */
    public boolean insert(PreMappedStatement preMappedStatement) {
        int flag = (int) data.dataAdd(preMappedStatement);
        return flag == 1;
    }

    /**
     * 修改方法
     *
     * @param preMappedStatement
     * @return
     */
    public boolean update(PreMappedStatement preMappedStatement) {
        int flag = (int) data.dataAdd(preMappedStatement);
        return flag == 1;
    }

}
