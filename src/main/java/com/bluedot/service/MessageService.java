package com.bluedot.service;

import com.alibaba.fastjson.JSON;
import com.bluedot.controller.DistributeClass;
import com.bluedot.dao.jedis.JedisUtil;
import com.bluedot.pojo.pack.PreMappedStatement;
import com.bluedot.pojo.pack.ResultEntity;
import com.bluedot.pojo.vo.MessageVO;
import com.bluedot.pojo.vo.UserVO;
import redis.clients.jedis.Jedis;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 发送信息类
 */
public class MessageService extends DistributeClass {
    private Map<String,String> map;
    private List<String> newList ;
    private Jedis jedis;

    public MessageService(PreMappedStatement param) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        super(param);
        this.jedis = new JedisUtil().getJedis();
        this.map = (Map) JSON.parse((String) param.getData());
        String[] split = preMappedStatement.getMethodType().split("/");
        switch (split[split.length - 1]){
            case "addOneMessage":
                addMessage();
                break;
            case "queryMessage":
                queryMessage();
                break;
            case "deleteMessage":
                deleteMessage();
                break;
            default:
                break;
        }
    }

    private void addMessage(){
        String userId = map.get("userId");
        String content = map.get("content");
        if (userId == null || content == null){
            throw new RuntimeException("未获取用户id和内容");
        }
        LocalDateTime localDateTime = LocalDateTime.now();
        String time = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        jedis.lpush(userId+"newList", content+"/"+time);
    }

    private void queryMessage(){
        String userId = map.get("userId");
        if (userId == null){
            getResponseQueue().add(ResultEntity.falseWithoutData("未获得用户id"));
            return;
        }
        List<String> messages = jedis.lrange(userId + "newList",0,-1);
        List<MessageVO> messageVOS = new ArrayList<>();
        if (messages.size() >= 5){
            // 每次只给5条信息
            for (int i = 0; i < 5; i++) {
                String s = messages.get(i);
                if (s == null){
                    break;
                }else {
                    String[] split = s.split("/");
                    messageVOS.add(new MessageVO(split[0],split[1]));
                }
            }
            getResponseQueue().add(ResultEntity.successWithData(messageVOS));
        }else {
            for (String s:
                 messages) {
                String[] split = s.split("/");
                messageVOS.add(new MessageVO(split[0],split[1]));
            }
            getResponseQueue().add(ResultEntity.successWithData(messageVOS));
        }
    }

    private void deleteMessage(){
        String userId = map.get("userId");
        String content = map.get("content");
        if (userId == null || content == null){
            throw new RuntimeException("未获取用户id和内容");
        }
        Long lrem = jedis.lrem(userId + "newList", 0, content);
        if (lrem != 1){
            throw new RuntimeException("删除数据失败");
        }
    }
}
