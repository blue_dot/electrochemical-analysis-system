package com.bluedot.service;

import com.bluedot.controller.DistributeClass;
import com.bluedot.pojo.pack.PreMappedStatement;
import com.bluedot.pojo.pack.ResultEntity;
import com.bluedot.pojo.vo.RequestVO;
import com.bluedot.pojo.vo.UserVO;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * 请求业务类
 */
public class RequestService extends DistributeClass {

    private UtilTool util;

    private RequestVO requestVO;
    public RequestService(PreMappedStatement preMappedStatement) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        super(preMappedStatement);
        this.util = new UtilTool();
        this.requestVO = (RequestVO) preMappedStatement.getData();
        String[] split = preMappedStatement.getMethodType().split("/");
        switch (split[split.length - 1]){
            case "updateRequest":
                updateRequest();
            case "insertLogin":
                insertLogin();
            default:
                break;
        }

    }

    /**
     * 申请非法登录解冻按钮
     */
    private void insertLogin(){
        if (util == null){
            this.util = new UtilTool();
        }
        // 1. 获得相关信息
        RequestVO data = (RequestVO) preMappedStatement.getData();
        // 2. 获得邮箱，并查找用户
        String email = data.getUserId();
        data.setUserId(null);
        UserVO userVO = new UserVO();
        userVO.setUserEmail(email);
        List<Object> query = util.query(new PreMappedStatement<>(UtilTool.QUERY, userVO));
        if (query.size() != 1){
            getResponseQueue().add(ResultEntity.falseWithoutData("无用户"));
        }else {
            userVO = (UserVO) query.get(0);
        }
        // 3. 设置用户正常
        userVO.setUserStatus(0);
        boolean update = util.update(new PreMappedStatement<>(UtilTool.UPDATE, userVO));
        if (!update){
            getResponseQueue().add(ResultEntity.falseWithoutData("用户恢复失败"));
        }else {
            getResponseQueue().add(ResultEntity.successWithData("用户恢复成功"));
        }
    }
    /**
     * 管理员审核请求
     */
    private void updateRequest() {
        RequestVO data = (RequestVO) this.preMappedStatement.getData();
        // 1. 判断请求
        Integer requestState = data.getRequestState();
        data.setRequestState(null);
        List<Object> query = util.query(new PreMappedStatement<>(UtilTool.QUERY, data));
        if (query.size() != 1){
            throw new RuntimeException("请求错误");
        }else {
            data = (RequestVO) query.get(0);
        }
        if (requestState == 1){
            // 2. 管理员通过请求
            // 3. 修改用户状态
            UserVO userVO = new UserVO();
            userVO.setUserId(data.getUserId());
            userVO.setUserStatus(0);
            boolean update = util.update(new PreMappedStatement<>(UtilTool.UPDATE, userVO));
            if (!update){
                throw new RuntimeException("用户修改失败");
            }else {
                // 4. 删除请求
                boolean delete = util.delete(new PreMappedStatement<>(UtilTool.DELETE, data));
                if (!delete){
                    throw new RuntimeException("请求删除失败");
                }
            }
        }else {
            // 2. 管理员不通过请求
            // 3. 删除请求
            boolean delete = util.delete(new PreMappedStatement<>(UtilTool.DELETE, data));
            if (!delete){
                throw new RuntimeException("请求删除失败");
            }
        }
    }

    /**
     * 修改请求
     * @return
     */
    @Override
    protected boolean update() {
        if (util == null){
            this.util = new UtilTool();
        }
        boolean update = super.update();
        if (!update){
            throw new RuntimeException("请求修改失败");
        }
        return true;
    }

    /**
     * 添加请求
     * @return
     */
    @Override
    protected boolean insert() {
        if (util == null){
            this.util = new UtilTool();
        }
        RequestVO data = (RequestVO) preMappedStatement.getData();
        String email = data.getUserId();
        data.setUserId(null);
        UserVO userVO = new UserVO();
        userVO.setUserEmail(email);
        List<Object> query = util.query(new PreMappedStatement<>(UtilTool.QUERY, userVO));
        if (query.size() != 1){
            throw new RuntimeException("无用户");
        }else {
            UserVO o = (UserVO) query.get(0);
            data.setUserId(o.getUserId());
        }

        data.setRequestState(0);
        boolean insert = super.insert();
        if (!insert){
            throw new RuntimeException("请求插入失败");
        }
        return true;
    }

    /**
     * 查询请求
     * @return
     */
    @Override
    protected List query() {
        if (preMappedStatement.getMethodType().equals("query")){
            return null;
        }
        if (util == null){
            this.util = new UtilTool();
        }
        // 1.查询对应数组
        List query = super.query();
        if (query == null){
            getResponseQueue().add(ResultEntity.falseWithoutData("查询失败"));
            return null;
        }
        // 2.封装队列
        else {
            List<RequestVO> requestVOS = new ArrayList<>();
            for (Object request : query) {
                requestVOS.add((RequestVO) request);
            }
            getResponseQueue().add(ResultEntity.successWithData(requestVOS));
            return requestVOS;
        }
    }

    /**
     * 请求删除
     * @return
     */
    @Override
    protected boolean delete() {
        boolean delete = super.delete();
        if (!delete){
            throw new RuntimeException("请求删除失败");
        }
        return true;
    }
}
