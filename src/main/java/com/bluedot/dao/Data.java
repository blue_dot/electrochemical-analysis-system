package com.bluedot.dao;

import com.bluedot.pojo.pack.PreMappedStatement;

import java.sql.Connection;

/**
 * dao操作类
 *
 * @author chenzou'quan
 */
public class Data extends DataQueue {

    public Object dataAdd(PreMappedStatement param) {
        // 1. 队列管道
        // return add(param);
        // 2. 普通管道
        return addData(param);
    }


    public Object dataAddForBackups(PreMappedStatement param, Connection connection) {
        return addBackups(param, connection);
    }
}
