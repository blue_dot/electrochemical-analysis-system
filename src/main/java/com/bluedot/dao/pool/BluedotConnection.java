package com.bluedot.dao.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 连接类
 * 保存单个数据库连接
 * 记录该连接开始空闲时间和目前空闲时间
 */
public class BluedotConnection {
    /**
     * 数据库连接
     */
    private Connection connection=null;
    /**
     * 开始空闲时间
     */
    private long startIdleTime;
    /**
     * 目前空闲时间
     */
    private long nowIdleTime;

    /**
     * 构造函数
     * 获得数据库连接和该连接的开始空闲时间
     */
    public BluedotConnection(String driver, String url, String username, String password){
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, username, password);
            startIdleTime=System.currentTimeMillis();
        } catch (Exception e) {
            System.out.println("连接获取失败!");
            System.exit(0);
        }
    }

    public BluedotConnection(Connection connection){
        this.connection=connection;
        startIdleTime=System.currentTimeMillis();
    }

    /**
     * 提供获得数据库连接的方法
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * 提供设置开始空闲时间的方法
     */
    public void setStartIdleTime() {
        this.startIdleTime = System.currentTimeMillis();
    }

    /**
     * 提供获得目前空闲时间的方法
     */
    public long getNowIdleTime() {
        nowIdleTime=System.currentTimeMillis()-this.startIdleTime;
        return nowIdleTime;
    }

    /**
     * 提供关闭数据库连接的方法
     */
    public void closeConnection(){
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("连接关闭异常!");
            System.exit(0);
        }
    }
}