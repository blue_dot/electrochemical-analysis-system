package com.bluedot.dao.pool;
import java.sql.Connection;

/**
 * 连接池管理类
 * 含有唯一的连接池类
 * 只负责连接的获取和释放
 */
public class BluedotConnectionPoolManage {
    /**
     * 唯一的连接池类
     */
    private static BluedotConnectionPool connectionPool=new BluedotConnectionPool();

    /**
     * 获取数据库连接的方法
     */
    public static synchronized Connection getConnection() {
        return connectionPool.getConnection();
    }

    /**
     * 释放数据库连接的方法
     */
    public static synchronized void releaseConnection(Connection connection) {
        connectionPool.releaseConnection(connection);
    }

    /**
     * 已经分配连接数
     * @return
     */
    public Integer getBusyConnectionNum() {
        return connectionPool.getBusyConnectionNum();
    }

    /**
     * 获取空闲连接数
     * @return
     */
    public static Integer getIdleConnectionNum() {
        return connectionPool.getIdleConnectionNum();
    }
    /**
     * 主方法
     */
    public static void main(String[] args) {
        BluedotConnectionPoolManage connectionPoolManage = new BluedotConnectionPoolManage();
        System.out.println("连接池创建成功!");
    }
}
