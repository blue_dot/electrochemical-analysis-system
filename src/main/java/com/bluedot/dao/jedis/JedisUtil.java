package com.bluedot.dao.jedis;

import com.bluedot.utils.ResourceUtil;
import redis.clients.jedis.Jedis;

import java.util.Properties;

/**
 * redis工具类
 *
 * @author chenzou'quan
 */
public class JedisUtil {
    private Jedis jedis;

    /**
     * redis初始化方法
     */
    public JedisUtil() {
        this.jedis = new Jedis(ResourceUtil.REDIS_URL);
        // 默认账号（即以实例ID命名的账号）：直接填写密码即可。
        // 新创建的账号：密码格式为<user>:<password>。例如自定义账号为testaccount，密码为Rp829dlwa，密码需填写为testaccount:Rp829dlwa。
        jedis.auth(ResourceUtil.REDIS_USER_PASSWORD);
        String ping = jedis.ping();
        if (ping == null) {
            throw new RuntimeException("Redis链接失败");
        }
    }

    public Jedis getJedis() {
        return jedis;
    }
}
