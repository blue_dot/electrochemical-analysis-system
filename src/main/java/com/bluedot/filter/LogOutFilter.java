package com.bluedot.filter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bluedot.dao.jedis.JedisUtil;
import com.bluedot.pojo.pack.ResultEntity;
import com.bluedot.pojo.vo.UserVO;
import com.bluedot.service.Token;
import io.jsonwebtoken.Claims;
import redis.clients.jedis.Jedis;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

public class LogOutFilter implements Filter {
    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        // 1.获取token
        String token = ((HttpServletRequest) request).getHeader("token");
        // 2.把token放入Redis黑名单
        Jedis jedis = new JedisUtil().getJedis();
        jedis.set(token, token);
        // 2.1设置过期时间
        jedis.expire(token,3600000);
        // 3.设置session为空
        HttpSession session = ((HttpServletRequest) request).getSession();
        session.setAttribute("user",null);
        // 5.写入响应包中
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().println(JSON.toJSONString(ResultEntity.successWithoutData()));
        response.getWriter().flush();
    }
}
