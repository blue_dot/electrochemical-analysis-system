package com.bluedot.pojo.bo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.bluedot.pojo.vo.LogVo;
import com.bluedot.excel.DateConverter;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 *<p>描述: 日志excel类<p>
 *@author QiYu
 */
@Data
public class ExcelLog implements Serializable {

    @ExcelIgnore
    private static final long serialVersionUID = 1L;

    /**
     * 日志编号
     */
    @ExcelProperty(value = "日志编号")
    private Integer logId;

    /**
     * 日志状态,0表示操作成功，1表示操作失败
     */
    @ExcelProperty(value = "日志状态")
    private Integer logState;

    /**
     * ip地址，建议使用无符号int类型
     */
    @ExcelProperty(value = "ip地址")
    private String handerIp;

    /**
     * 用户名称
     */
    @ExcelProperty(value = "用户名称")
    private String userName;


    /**
     * 日志的描述
     */
    @ExcelProperty(value = "日志路径")
    private String logUrl;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间", converter = DateConverter.class)
    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    private Timestamp createTime;

    /**
     * 修改时间
     */
    @ExcelProperty(value = "修改时间", converter = DateConverter.class)
    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    private Timestamp updateTime;

    public ExcelLog(LogVo log) {
        this.logId=log.getLogId();
        this.logUrl=log.getLogUrl();
        this.logState=log.getLogState();
        this.updateTime=log.getUpdateTime();
        this.createTime=log.getCreateTime();
        this.userName=log.getUserName();
        this.handerIp=log.getHanderIp();
    }

}
