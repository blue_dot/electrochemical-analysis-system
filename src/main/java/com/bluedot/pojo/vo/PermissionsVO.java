package com.bluedot.pojo.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PermissionsVO {
    private Integer permissionsId;
    private String permissionsName;
    private String permissionsUrl;
    private Integer roleId;

    public PermissionsVO(Integer permissionsId, String permissionsName, String permissionsUrl) {
        this.permissionsId = permissionsId;
        this.permissionsName = permissionsName;
        this.permissionsUrl = permissionsUrl;
    }

    public PermissionsVO(Integer roleId) {
        this.roleId = roleId;
    }

    public PermissionsVO(Integer permissionsId, Integer roleId) {
        this.permissionsId = permissionsId;
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "PermissionsVO{" +
                "permissionsId=" + permissionsId +
                ", permissionsName='" + permissionsName + '\'' +
                ", permissionsUrl='" + permissionsUrl + '\'' +
                '}';
    }
}
