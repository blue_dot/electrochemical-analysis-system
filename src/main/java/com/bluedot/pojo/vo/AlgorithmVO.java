package com.bluedot.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlgorithmVO {
    public Integer algorithmId;
    private String algorithmName;
    private Integer hotNumber;
    private String algorithmDescribe;
    private String algorithmUrl;
    private String userId;
    private Integer algorithmState;
    private Integer algorithmDelete;
    private Integer algorithmCheck;
    private String algorithmType;
    private Integer boxTest;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
    private String algorithmExe;

}
