package com.bluedot.pojo.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * 角色VO类
 *
 * @author chenzou'quan
 */
@Getter
@Setter
@NoArgsConstructor
public class RoleVO {
    private Integer roleId;
    private String roleName;
    private String roleContent;
    private List permissions;

    private String userId;

    public RoleVO(Integer roleId, String roleName, String roleContent, List<String> permissions) {
        this.roleId = roleId;
        this.roleName = roleName;
        this.roleContent = roleContent;
        this.permissions = permissions;
    }

    public RoleVO(Integer roleId) {
        this.roleId = roleId;
    }

    public RoleVO(String tmpUserID) {
        this.userId = tmpUserID;
    }

    @Override
    public String toString() {
        return "RoleVO{" +
                "roleId=" + roleId +
                ", roleName='" + roleName + '\'' +
                ", roleContent='" + roleContent + '\'' +
                ", permissions=" + permissions +
                '}';
    }
}
