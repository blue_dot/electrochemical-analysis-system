package com.bluedot.pojo.vo;

import lombok.Data;

@Data
public class MessageVO {
    private String content;
    private String time;

    public MessageVO(String content, String time) {
        this.content = content;
        this.time = time;
    }
}
