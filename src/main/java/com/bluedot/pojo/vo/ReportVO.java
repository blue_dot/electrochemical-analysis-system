package com.bluedot.pojo.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.bluedot.pojo.po.Point;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportVO {

    private Integer reportId;
    private Double maec;
    private Double maep;
    private String materialName;
    private Double rcSqrt;
    private String reportName;
    private Integer reportState;
    private String resultModel;
    private String reportDescribe;
    private Double rmsec;
    private Double rmsep;
    private Double rpd;
    private Double rpSqrt;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    private String userId;
    private Integer materialId;
    private String algorithmName; // 预处理算法名称
    private String modelName; // 建模算法

    // 数据库数据
    private String trainSolubility;// 训练集溶度
    private String trainIp;// 训练集电流
    private String trainPreSolubility;// 训练集预测溶度
    private String testSolubility;// 测试集溶度
    private String testIp;// 测试集电流
    private String testPreSolubility;// 测试集预测溶度
    private String trainEquation;// 测试集回归方程
    private String testEquation;// 训练集回归方程
    // 前端展示
    private String[] trainSolubilityShow;// 训练集溶度
    private String[] trainIpShow;// 训练集电流
    private String[] trainPreSolubilityShow;// 训练集预测溶度
    private String[] testSolubilityShow;// 测试集溶度
    private String[] testIpShow;// 测试集电流
    private String[] testPreSolubilityShow;// 测试集预测溶度
    // 图片坐标
    private List<List<String>> trainPointCoordinate;// 训练集图片坐标
    private List<List<String>> testPointCoordinate;// 测试集图片坐标




    @Override
    public String toString() {
        return reportName + ":{" +
                "reportId=" + reportId +
                '\'' +
                ", maec=" + maec +
                ", maep=" + maep +
                ", materialName='" + materialName + '\'' +
                ", rcSqrt=" + rcSqrt +
                ", reportName='" + reportName + '\'' +
                ", reportState=" + reportState +
                ", resultModel='" + resultModel + '\'' +
                ", reportDescribe='" + reportDescribe + '\'' +
                ", rmsec=" + rmsec +
                ", rmsep=" + rmsep +
                ", rpd=" + rpd +
                ", rpSqrt=" + rpSqrt +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", userId='" + userId + '\'' +
                "};";
    }
}
