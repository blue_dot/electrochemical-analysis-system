package com.bluedot.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndexNumVO {

    private Integer experimentNum;
    private Integer reportNum;
    private Integer algorithmNum;

}
