package com.bluedot.pojo.vo;


import com.alibaba.fastjson.annotation.JSONField;
import com.bluedot.pojo.po.Point;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class MaterialVO {

    private Integer materialId;
    private String bufferSolution;// 缓冲溶液
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    private Double materialEp;
    private Double materialIp;
    private String materialName;
    private String materialPoints;// 给数据库的
    private String materialRemarks;
    private Double materialSolubility;// 物质溶度
    private Integer materialState;
    private Integer state;
    private String materialType;
    private Integer materialTypeId;
    private List<Point> points;// 前端展示
    private List<Point> originPoint;// 前端展示——原始数据
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    private String userId;
    private String rightPoint;// 从前端解析出字符串，后端转换
    private String leftPoint;
    private Integer materialSpecificId; // 数据详情 id
    private String algorithmName; // 使用算法
    private Integer algorithmId; // 使用算法 id


    public MaterialVO(Integer materialId) {
        this.materialId = materialId;
    }

    public MaterialVO(String materialType, Integer materialTypeId, String materialName, Double materialSolubility, LocalDateTime createTime, String bufferSolution, String materialRemarks) {
        this.materialType = materialType;
        this.materialTypeId = materialTypeId;
        this.materialName = materialName;
        this.materialSolubility = materialSolubility;
        this.createTime = createTime;
        this.bufferSolution = bufferSolution;
        this.materialRemarks = materialRemarks;
    }

    @Override
    public String toString() {
        return materialName + ":{" +
                "bufferSolution='" + bufferSolution + '\'' +
                ", createTime=" + createTime +
                ", materialEp=" + materialEp +
                ", materialId=" + materialId +
                ", materialIp=" + materialIp +
                ", materialName='" + materialName + '\'' +
                ", materialPoints='" + materialPoints + '\'' +
                ", materialRemarks='" + materialRemarks + '\'' +
                ", materialSolubility=" + materialSolubility +
                ", materialState=" + materialState +
                ", materialType='" + materialType + '\'' +
                ", materialTypeId=" + materialTypeId +
                ", points='" + points + '\'' +
                ", updateTime=" + updateTime +
                ", userId='" + userId + '\'' +
                "};";
    }
}