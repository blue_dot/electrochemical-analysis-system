package com.bluedot.pojo.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户视图类
 *
 * @author chenzou'quan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVO {
    private String userId;
    private String userName;
    private String userPassword;
    private String userSex;
    private String userPhone;
    private String userAvatar;
    private String userEmail;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 0：正常使用
     * 1：非法登录冻结状态；
     * 2：非法操作冻结状态；
     * 3：逻辑删除状态
     * 4：禁用
     */
    private Integer userStatus;
    private List<RoleVO> roles;
    private List<String> urls;

    public UserVO(String userId, String userName, String userPassword, String userSex, String userPhone, String userAvatar, String userEmail, Integer userStatus, List<RoleVO> roles, List<String> urls) {
        this.userId = userId;
        this.userName = userName;
        this.userPassword = userPassword;
        this.userSex = userSex;
        this.userPhone = userPhone;
        this.userAvatar = userAvatar;
        this.userEmail = userEmail;
        this.userStatus = userStatus;
        this.roles = roles;
        this.urls = urls;
    }
}
