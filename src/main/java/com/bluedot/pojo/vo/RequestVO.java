package com.bluedot.pojo.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
/**
 * 申请请求实体类
 */
public class RequestVO {
    private Integer requestId;
    private String requestContent;
    private String materialTypeName;//新物质类型的名称
    private String userId;
    /**
     * 请求类型
     * 0：解冻
     * 1：上传算法
     * 2：上传物质类型
     * 3：数据还原
     */
    private Integer requestType;
    private String requestHandlerId;
    @JSONField(format = "yyyy-MM-dd HH-mm-ss")
    private LocalDateTime createTime;
    @JSONField(format = "yyyy-MM-dd HH-mm-ss")
    private LocalDateTime updateTime;

    private Integer requestState;

    public RequestVO(Integer requestId, String requestContent, String userId, Integer requestType, LocalDateTime createTime, LocalDateTime updateTime) {
        this.requestId = requestId;
        this.requestContent = requestContent;
        this.userId = userId;
        this.requestType = requestType;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public RequestVO(Integer requestId, String requestContent, String userId, Integer requestType, Integer requestState) {
        this.requestId = requestId;
        this.requestContent = requestContent;
        this.userId = userId;
        this.requestType = requestType;
        this.requestState = requestState;
    }

    public RequestVO(String requestHandlerId) {
        this.requestHandlerId = requestHandlerId;
    }

    public RequestVO(Integer requestType) {
        this.requestType = requestType;
    }

    public RequestVO(Integer requestId, String requestContent, String userId, Integer requestType) {
        this.requestId = requestId;
        this.requestContent = requestContent;
        this.userId = userId;
        this.requestType = requestType;
    }


    public RequestVO(Integer requestId, String requestHandlerId, Integer requestState) {
        this.requestId = requestId;
        this.requestHandlerId = requestHandlerId;
        this.requestState = requestState;
    }

    public RequestVO setRequestId(Integer requestId) {
        this.requestId = requestId;
        return this;
    }
}
