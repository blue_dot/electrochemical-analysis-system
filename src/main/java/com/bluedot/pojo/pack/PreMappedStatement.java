package com.bluedot.pojo.pack;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;

/**
 * 包装消息类
 *
 * @author chenzou'quan
 */
public class PreMappedStatement<T> {
    private String methodType;

    private T data;

    private InputStream file;

    private HttpServletRequest request;

    public HttpServletRequest getRequest() {
        return request;
    }

    public PreMappedStatement(String methodType, T data, HttpServletRequest request) {
        this.methodType = methodType;
        this.data = data;
        this.request = request;
    }

    public PreMappedStatement(String methodType, T data, InputStream file, HttpServletRequest request) {
        this.methodType = methodType;
        this.data = data;
        this.file = file;
        this.request = request;
    }

    public PreMappedStatement(String methodType,InputStream file, HttpServletRequest request) {
        this.methodType = methodType;
        this.file = file;
        this.request = request;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setMethodType(String methodType) {
        this.methodType = methodType;
    }

    public PreMappedStatement(String methodType, T data) {
        this.methodType = methodType;
        this.data = data;
    }

    public String getMethodType() {
        return methodType;
    }

    public T getData() {
        return data;
    }

    public InputStream getFile() {
        return file;
    }
}
