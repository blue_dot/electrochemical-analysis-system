package com.bluedot.pojo.pack;

/**
 * 返回构造类
 *
 * @author chenzou'quan
 */
public class ResultEntity<T> {

    public static final String SUCCESS = "SUCCESS";
    public static final String FALSE = "FALSE";
    public static final String NO_MESSAGE = "NO_MESSAGE";
    public static final String NO_DATE = "NO_DATE";

    private String result;
    private String message;
    private T data;
    private String token;

    public static <E> ResultEntity<E> successWithoutData() {

        return new ResultEntity(SUCCESS,NO_MESSAGE,null);
    }

    public static <E> ResultEntity<E> falseWithoutData(String message){
        return new ResultEntity(FALSE, message, null);
    }

    public static <E> ResultEntity<E> successWithData(E data) {

        return new ResultEntity(SUCCESS, NO_MESSAGE, data);
    }

    public static <E> ResultEntity<E> successWithDataAndToken(E data,String token){
        return new ResultEntity(SUCCESS,NO_MESSAGE,data,token);
    }

    public ResultEntity() {
    }

    public ResultEntity(String result, String message, T data, String token) {
        this.result = result;
        this.message = message;
        this.data = data;
        this.token = token;
    }

    public ResultEntity(String result, String message, T data) {
        this.result = result;
        this.message = message;
        this.data = data;
    }

    public static void FALSE(String message) {
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        if (this.token == null){
            return "ResultEntity{" +
                    "result='" + result + '\'' +
                    ", message='" + message + '\'' +
                    ", data=" + data +
                    '}';
        }else{
            return  "ResultEntity{" +
                    "result='" + result + '\'' +
                    ", message='" + message + '\'' +
                    ", data=" + data +'\'' +
                    ", token='" + token + '\'' +
                        '}';

        }
    }
}