package com.bluedot.pojo.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MaterialDetail {


    private Integer materialSpecificId;// 数据详情 id
    private Integer algorithmId;
    private String algorithmName;
    private String highPoint;
    private String leftPoint;
    private Double materialEp;
    private Integer materialId;// 物质数据 id
    private Double materialIp;
    private String materialPoints;
    private Integer materialState;
    private String rightPoint;


    public MaterialDetail(Integer materialState, String result, Integer materialId, Integer algorithm_id, String algorithm_name) {
        this.materialState = materialState;
        this.materialPoints = result;
        this.materialId = materialId;
        this.algorithmId = algorithm_id;
        this.algorithmName = algorithm_name;

    }

    public MaterialDetail(Integer materialId) {
        this.materialId = materialId;
    }
}