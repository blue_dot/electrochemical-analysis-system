package com.bluedot.pojo.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportDetail {

    private Integer reportId;
    private Double maec;
    private Double maep;
    private Double rcSqrt;
    private Double rmsec;
    private Double rmsep;
    private Double rpd;
    private Double rpSqrt;

    // 数据库数据
    private String trainSolubility;// 训练集溶度
    private String trainIp;// 训练集电流
    private String trainPreSolubility;// 训练集预测溶度
    private String testSolubility;// 测试集溶度
    private String testIp;// 测试集电流
    private String testPreSolubility;// 测试集预测溶度
    private String trainEquation;// 测试集回归方程
    private String testEquation;// 训练集回归方程

    public ReportDetail(Double maec, Double maep, Double rcSqrt, Double rmsec,
                        Double rmsep, Double rpd, Double rpSqrt, String trainSolubility,
                        String trainIp, String trainPreSolubility, String testSolubility,
                        String testIp, String testPreSolubility, String trainEquation,
                        String testEquation) {
        this.maec = maec;
        this.maep = maep;
        this.rcSqrt = rcSqrt;
        this.rmsec = rmsec;
        this.rmsep = rmsep;
        this.rpd = rpd;
        this.rpSqrt = rpSqrt;
        this.trainSolubility = trainSolubility;
        this.trainIp = trainIp;
        this.trainPreSolubility = trainPreSolubility;
        this.testSolubility = testSolubility;
        this.testIp = testIp;
        this.testPreSolubility = testPreSolubility;
        this.trainEquation = trainEquation;
        this.testEquation = testEquation;
    }

    public ReportDetail(Integer reportId) {
        this.reportId = reportId;
    }
}