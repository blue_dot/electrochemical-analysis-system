package com.bluedot.pojo.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色实体类
 * @author chenzou'quan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RolePO {
    private Integer roleId;
    private String roleName;
    private String roleContent;
}
