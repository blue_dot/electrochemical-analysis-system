package com.bluedot.pojo.po;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Algorithm {

    private Integer algorithm_id;
    private String algorithm_name;
    private Integer hot_number;
    private String algorithm_describe;
    private String algorithm_url;
    private String user_id;
    private Integer algorithm_state;
    private String algorithm_type;
    private Integer box_test;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime create_time;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime update_time;
    private String algorithm_exe;// 算法文件所属类型

  /*  public Integer algorithmId;
    private String algorithmName;
    private Integer hotNumber;
    private String algorithmDescribe;
    private String algorithmUrl;
    private String userId;
    private Integer algorithmState;
    private Integer algorithmDelete;
    private Integer algorithmCheck;
    private String algorithmType;
    private Integer boxTest;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    private String algorithmExe;*/

}
