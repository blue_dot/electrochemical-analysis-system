package com.bluedot.pojo.po;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @ClassName: MaterialType
 * @Description: TODO
 * @Author: 初久
 * @Date: 2022/8/15 10:28
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MaterialType {
    private Integer materialTypeId;
    @JSONField(format = "yyyy-MM-dd HH-mm-ss")
    private LocalDateTime createTime;
    private String materialTypeContent;
    private String materialTypeName;
    @JSONField(format = "yyyy-MM-dd HH-mm-ss")
    private LocalDateTime updateTime;

    public MaterialType(Integer materialTypeId) {
        this.materialTypeId = materialTypeId;
    }

    public MaterialType(String materialTypeContent, String materialTypeName) {
        this.materialTypeContent = materialTypeContent;
        this.materialTypeName = materialTypeName;
    }
}
