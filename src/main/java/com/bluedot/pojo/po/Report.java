package com.bluedot.pojo.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @ClassName: Report
 * @Description: TODO
 * @Author: 初久
 * @Date: 2022/8/15 10:14
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Report {
    private Integer reportId;
    private LocalDateTime createTime;
    private Integer materialId;
    private String materialName;
    private String reportName;
    private Integer reportState;
    private LocalDateTime updateTime;
    private String reportDescribe;
    private String userId;
    private String algorithmName; // 预处理算法名称
    private String modelName; // 建模算法

    public Report(Integer reportId) {
        this.reportId = reportId;
    }

    public Report(Integer reportId, Integer reportState) {
        this.reportId = reportId;
        this.reportState = reportState;
    }

    public Report setReportState(Integer reportState) {
        this.reportState = reportState;
        return this;
    }
}
