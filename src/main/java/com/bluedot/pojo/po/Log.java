package com.bluedot.pojo.po;

import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.bluedot.excel.DateConverter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 *<p>描述:日志类 <p>
 *@author QiYu
 *@CreateDate 2022/8/14 9:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Log {
    private Integer logId;
    private Integer logState;
    private String logUrl;
    private String handerId;
    private String handerIp;
    private Timestamp createTime;
    private Timestamp updateTime;
}
