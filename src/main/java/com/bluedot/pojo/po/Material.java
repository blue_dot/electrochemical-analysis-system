package com.bluedot.pojo.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Material {

    private Integer materialId;
    private String bufferSolution;
    private LocalDateTime createTime;
    private String materialName;
    private String materialRemarks;
    private Double materialSolubility;
    private String materialType;
    private Integer materialTypeId;
    private LocalDateTime updateTime;
    private String userId;// 用户 id
    private Integer state;

    public Material(Integer materialId, Integer state) {
        this.materialId = materialId;
        this.state = state;
    }

    public Material(Integer materialId) {
        this.materialId = materialId;
    }

    public Material setState(Integer state) {
        this.state = state;
        return this;
    }
}