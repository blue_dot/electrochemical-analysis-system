package com.bluedot.controller;

import com.alibaba.fastjson.JSON;
import com.bluedot.pojo.pack.FileResponse;
import com.bluedot.pojo.pack.PreMappedStatement;
import com.bluedot.pojo.pack.ResultEntity;
import com.bluedot.pojo.po.Log;
import com.bluedot.pojo.po.User;
import com.bluedot.pojo.vo.UserVO;
import com.bluedot.service.UtilTool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


/**
 * @author chenzou'quan
 */
@WebServlet(name = "Controller", urlPatterns = "/*")
public class Controller extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 1. 获取路由信息
        StringBuffer url = req.getRequestURL();
        // 2. 获取JSON数据
        String jsonString;
        if (req.getMethod().equals("POST")) {
            jsonString = GetRequestJsonUtils.getRequestJsonString(req).replace('\n', ' ');
        } else {
            jsonString = null;
        }
        // 3. 初始化PreMappedStatement
        PreMappedStatement<String> statement = new PreMappedStatement<>(url.toString(), jsonString, req);
        // 4. 添加到请求队列中去
        ResultEntity resultEntity = logInsert(statement, req);

        // 5. 设置响应包
        resp.setContentType("application/json;charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        //      判断data是不是文件封装，如果是的话，设置响应包的格式
        if (resultEntity.getData() instanceof FileResponse) {
            uploadFile((FileResponse) resultEntity.getData(), resp);
            resp.addHeader("fileName", ((FileResponse) resultEntity.getData()).getFileName());
            return;
        }
        resp.getWriter().println(JSON.toJSONString(resultEntity));
        resp.getWriter().flush();
    }


    /**
     * <p>描述: 下载文件方法<p>
     *
     * @author QiYu
     * @CreateDate 2022/8/17 21:28
     */
    private void uploadFile(FileResponse responseEntity, HttpServletResponse resp) {
        resp.setContentType("text/html;charset=utf-8");
        // 获取文件名
        String filename = responseEntity.getFileName();
        //设置文件名的编码
        try {
            filename= URLEncoder.encode(filename, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        // 通知浏览器以下载的方式打开
        resp.addHeader("Content-Type", "application/octet-stream");
        resp.addHeader("Content-Disposition", "attachment;filename=" + filename);
        // 通过文件输入流读取文件
        OutputStream out = null;
        try {
            out = resp.getOutputStream();
            out.write(responseEntity.getFileBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }



    }


    /**
     * <p>描述:日志插入方法 <p>
     *
     * @param
     * @return
     * @author QiYu
     * @CreateDate 2022/9/7 11:00
     * @update [序号][日期YYYY-MM-DD][王震宇][变更描述]
     */
    private ResultEntity logInsert(PreMappedStatement preMappedStatement, HttpServletRequest req) {

        ResultEntity re = null;
        UtilTool utilTool = new UtilTool();

        String[] split = (preMappedStatement.getMethodType().split("/"));
        String s = split[split.length - 1];

//      凡是不是查询的一律插入日志，以及特殊接口getInfo
        if (!s.contains("query")&&!"getInfo".equals(s)) {
            //获取ip地址
            String ipv4 = req.getRemoteAddr();
            //将本机localhost改为127.0.0.1
            if ("0:0:0:0:0:0:0:1".equals(ipv4))ipv4 = "127.0.0.1";
//          获取请求路径
            String requestURI = req.getRequestURI();
//          获取sessionUser可能为空
            UserVO user = (UserVO)req.getSession().getAttribute("user");
//          创建log对象
            Log log = new Log();
            log.setLogUrl(requestURI);
            log.setHanderIp(ipv4);
            try {
                re = MessageQueue.addQueue(preMappedStatement);
            } catch (Exception e) {
//              将操作失败日志插入数据库,注意登录的时候报异常的话，不能加入到日志；
//              获取用户的id，这里判断非空因为登录失败没有存储session，所以要判断
                if (user!=null)log.setHanderId(user.getUserId());
//              设置日志状态为0
                log.setLogState(0);
//               登录的时候报异常不能加入到日志
                if (!s.contains("login")) utilTool.insert(new PreMappedStatement(UtilTool.INSERT, log));
                throw new RuntimeException(e);
            }
//          将没有异常的日志加入到日志
//          日志状态失败还是成功与否来设置日志状态
            if ("FALSE".equals(re.getResult())) log.setLogState(0);
            else log.setLogState(1);
//          获取用户id，用户不能为空；防止登录失败导致空指针异常
            if (user!=null) {
                log.setHanderId(user.getUserId());
                utilTool.insert(new PreMappedStatement(UtilTool.INSERT, log));
            }
            return re;
        }
//      不用插入日志
        try {
            return MessageQueue.addQueue(preMappedStatement);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}

