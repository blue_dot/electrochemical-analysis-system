package com.bluedot.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bluedot.pojo.pack.PreMappedStatement;
import com.bluedot.pojo.pack.ResultEntity;
import com.bluedot.pojo.vo.RequestVO;
import com.bluedot.pojo.vo.RoleVO;
import com.bluedot.pojo.vo.UserVO;
import com.bluedot.service.*;
import com.bluedot.utils.MessageUtil;

import java.lang.reflect.InvocationTargetException;
import java.util.List;


/**
 * 分发器
 *
 * @author chenzou'quan
 */
public class DistributeClass extends MessageQueue {
    /**
     * 子类实现参数
     */
    protected PreMappedStatement preMappedStatement;

    /**
     * 队列监控方法
     */
    protected static void update(boolean flag) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        PreMappedStatement param = flag ? getRequestQueue().remove() : getPreMappedStatements().remove();
        JSONObject object;
        String[] split = param.getMethodType().split("/");
        switch (split[split.length - 2]) {
            case "user":
                if (param.getData() != null && !"".equals(param.getData())) {
                    object = JSON.parseObject((String) param.getData());
                    if (object.getString("users") != null) {
                        String users = object.getString("users");
                        List<UserVO> userVOS = JSONObject.parseArray(users, UserVO.class);
                        param.setData(userVOS);
                    } else if (object.getString("code") != null) {
                        // 验证验证码
                        String code = object.getString("code");
                        String email = object.getString("userEmail");
                        if (MessageUtil.checkCode(code, email)) {
                            object.remove("code");
                            // 解析JSON数据
                            UserVO userVO = (UserVO) new UtilTool().JSONtoObject(object.toJSONString(), UserVO.class);
                            // 放入包装类
                            param.setData(userVO);
                        } else {
                            getResponseQueue().add(ResultEntity.falseWithoutData("验证码错误"));
                            break;
                        }
                    } else {
                        // 解析JSON数据
                        UserVO userVO = (UserVO) new UtilTool().JSONtoObject((String) param.getData(), UserVO.class);
                        // 放入包装类
                        param.setData(userVO);
                    }
                }
                new UserService(param);
                break;
            case "request":
                object = JSON.parseObject((String) param.getData());
                if (object.getString("code") != null) {
                    // 验证验证码
                    String code = object.getString("code");
                    String email = object.getString("userEmail");
                    if (MessageUtil.checkCode(code, email)) {
                        object.remove("code");
                        object.remove("userEmail");
                        object.put("userId",email);
                        // 解析JSON数据
                        RequestVO requestVO = (RequestVO) new UtilTool().JSONtoObject(object.toJSONString(), RequestVO.class);
                        // 放入包装类
                        param.setData(requestVO);
                    } else {
                        getResponseQueue().add(ResultEntity.falseWithoutData("验证码错误"));
                        break;
                    }
                } else {
                    // 解析JSON数据
                    RequestVO requestVO = (RequestVO) new UtilTool().JSONtoObject((String) param.getData(), RequestVO.class);
                    // 放入包装类
                    param.setData(requestVO);
                }
                new RequestService(param);
                break;
            case "backups":
                new BackupsService(param);
                break;
            case "log":
                new LogService(param);
                break;
            case "dataCharge":
                new DataChargeService(param);
                break;
            case "analyzedata":
                new AnalyzedataService(param);
                break;
            case "algorithm":
                new AlgorithmService(param);
                break;
            case "role":
                object = JSON.parseObject((String) param.getData());
                String roles = null;
                if (object != null) {
                    roles = object.getString("roles");
                }
                if (roles != null) {
                    List<RoleVO> roleVOS = JSONObject.parseArray(roles, RoleVO.class);
                    param.setData(roleVOS);
                } else {
                    RoleVO roleVO =null;
                    if ((param.getData()).equals("")){
                        roleVO  = new RoleVO();
                    }else {
                        // 解析JSON数据
                        roleVO = (RoleVO) new UtilTool().JSONtoObject((String) param.getData(), RoleVO.class);
                    }
                    // 放入包装类
                    param.setData(roleVO);
                }
                new RoleService(param);
                break;
            case "message":
                new MessageService(param);
                break;
            default:
                throw new RuntimeException("请求没有对应的实现类");
        }
    }

    /**
     * 普通缓冲监控方法
     */
    protected  void updateRequest(boolean flag) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        // 普通管道，true为同步，false为异步。
        PreMappedStatement param = flag ? this.request : getPreMappedStatements().remove();
        JSONObject object;
        String[] split = param.getMethodType().split("/");
        switch (split[split.length - 2]) {
            case "user":
                if (param.getData() != null && !"".equals(param.getData())) {
                    object = JSON.parseObject((String) param.getData());
                    if (object.getString("users") != null) {
                        String users = object.getString("users");
                        List<UserVO> userVOS = JSONObject.parseArray(users, UserVO.class);
                        param.setData(userVOS);
                    } else if (object.getString("code") != null) {
                        // 验证验证码
                        String code = object.getString("code");
                        String email = object.getString("userEmail");
                        if (MessageUtil.checkCode(code, email)) {
                            object.remove("code");
                            // 解析JSON数据
                            UserVO userVO = (UserVO) new UtilTool().JSONtoObject(object.toJSONString(), UserVO.class);
                            // 放入包装类
                            param.setData(userVO);
                        } else {
                            getResponseQueue().add(ResultEntity.falseWithoutData("验证码错误"));
                            break;
                        }
                    } else {
                        // 解析JSON数据
                        UserVO userVO = (UserVO) new UtilTool().JSONtoObject((String) param.getData(), UserVO.class);
                        // 放入包装类
                        param.setData(userVO);
                    }
                }
                new UserService(param);
                break;
            case "request":
                object = JSON.parseObject((String) param.getData());
                if (object.getString("code") != null) {
                    // 验证验证码
                    String code = object.getString("code");
                    String email = object.getString("userEmail");
                    if (MessageUtil.checkCode(code, email)) {
                        object.remove("code");
                        object.remove("userEmail");
                        object.put("userId",email);
                        // 解析JSON数据
                        RequestVO requestVO = (RequestVO) new UtilTool().JSONtoObject(object.toJSONString(), RequestVO.class);
                        // 放入包装类
                        param.setData(requestVO);
                    } else {
                        getResponseQueue().add(ResultEntity.falseWithoutData("验证码错误"));
                        break;
                    }
                } else {
                    // 解析JSON数据
                    RequestVO requestVO = (RequestVO) new UtilTool().JSONtoObject((String) param.getData(), RequestVO.class);
                    // 放入包装类
                    param.setData(requestVO);
                }
                new RequestService(param);
                break;
            case "backups":
                new BackupsService(param);
                break;
            case "log":
                new LogService(param);
                break;
            case "dataCharge":
                new DataChargeService(param);
                break;
            case "analyzedata":
                new AnalyzedataService(param);
                break;
            case "algorithm":
                new AlgorithmService(param);
                break;
            case "role":
                object = JSON.parseObject((String) param.getData());
                String roles = null;
                if (object != null) {
                    roles = object.getString("roles");
                }
                if (roles != null) {
                    List<RoleVO> roleVOS = JSONObject.parseArray(roles, RoleVO.class);
                    param.setData(roleVOS);
                } else {
                    RoleVO roleVO =null;
                    if ((param.getData()).equals("")){
                        roleVO  = new RoleVO();
                    }else {
                        // 解析JSON数据
                        roleVO = (RoleVO) new UtilTool().JSONtoObject((String) param.getData(), RoleVO.class);
                    }
                    // 放入包装类
                    param.setData(roleVO);
                }
                new RoleService(param);
                break;
            case "message":
                new MessageService(param);
                break;
            default:
                throw new RuntimeException("请求没有对应的实现类");
        }
    }

    /**
     * 普通管道初始化
     */
    public DistributeClass() {
    }

    protected DistributeClass(PreMappedStatement preMappedStatement) {
        this.preMappedStatement = preMappedStatement;
        String[] split = preMappedStatement.getMethodType().split("/");
        switch (split[split.length - 1]) {
            case "insert":
                insert();
                break;
            case "query":
                query();
                break;
            case "update":
                update();
                break;
            case "delete":
                delete();
                break;
        }
    }

    protected boolean insert() {
        preMappedStatement.setMethodType(UtilTool.INSERT);
        boolean insert = new UtilTool().insert(preMappedStatement);
        return insert;
    }

    protected List query() {
        preMappedStatement.setMethodType(UtilTool.QUERY);
        return new UtilTool().query(preMappedStatement);
    }

    protected boolean update() {
        preMappedStatement.setMethodType(UtilTool.UPDATE);
        return new UtilTool().update(preMappedStatement);
    }

    protected boolean delete() {
        preMappedStatement.setMethodType(UtilTool.DELETE);
        return new UtilTool().delete(preMappedStatement);
    }
}
