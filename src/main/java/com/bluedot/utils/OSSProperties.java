package com.bluedot.utils;

/**
 * OSS资源实体类
 */
//aliyun:
//  oss:
//    access-key-secret: nCOtPaTQw3CrnaLYFJz81qiJLZyzeM
//    access-key-id: LTAI5tM3ZUhe3g44AwKDy6D5
//    bucket-domain: chenzouquan.oss-cn-shenzhen.aliyuncs.com
//    bucket-name: chenzouquan
//    end-point: oss-cn-shenzhen.aliyuncs.com
public class OSSProperties {

    private String endPoint = "oss-cn-shenzhen.aliyuncs.com";
    private String bucketName = "chenzouquan";
    private String accessKeyId = "LTAI5tM3ZUhe3g44AwKDy6D5";
    private String accessKeySecret = "nCOtPaTQw3CrnaLYFJz81qiJLZyzeM";
    private String bucketDomain = "chenzouquan.oss-cn-shenzhen.aliyuncs.com";

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getBucketDomain() {
        return bucketDomain;
    }

    public void setBucketDomain(String bucketDomain) {
        this.bucketDomain = bucketDomain;
    }

    @Override
    public String toString() {
        return "OSSProperties{" +
                "endPoint='" + endPoint + '\'' +
                ", bucketName='" + bucketName + '\'' +
                ", accessKeyId='" + accessKeyId + '\'' +
                ", accessKeySecret='" + accessKeySecret + '\'' +
                ", bucketDomain='" + bucketDomain + '\'' +
                '}';
    }
}
