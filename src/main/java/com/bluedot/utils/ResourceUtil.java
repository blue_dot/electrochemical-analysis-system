package com.bluedot.utils;

import com.bluedot.dao.pool.BluedotConnectionPool;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ResourceUtil {
    /**
     * 数据库备份路径
     */
    public static String SQL_FILE_PATH;

    /**
     * 数据库名称
     */
    public static String ORIGIN_DATABASE;

    /***
     * Redis数据库路径
     */
    public static String REDIS_URL;

    /***
     * Redis数据库密码
     */
    public static String REDIS_USER_PASSWORD;

    /**
     * 算法路径
     */
    public static String ALGORITHM_URL;
    static {
        /**
         * 加载配置文件
         */
        Properties properties = new Properties();
        InputStream resourceAsStream = ResourceUtil.class.getClassLoader().getResourceAsStream("file.properties");
        try {
            properties.load(resourceAsStream);
            // 1. 备份数据库文件路径
            SQL_FILE_PATH = properties.getProperty("SQL_FILE_PATH");
            // 2. 数据库名称
            ORIGIN_DATABASE = properties.getProperty("ORIGIN_DATABASE");
            // 3. RedisURL
            REDIS_URL = properties.getProperty("REDIS_URL");
            // 4. Redis密码
            REDIS_USER_PASSWORD = properties.getProperty("REDIS_USER_PASSWORD");
            // 5. 算法路径
            ALGORITHM_URL = properties.getProperty("ALGORITHM_URL");
        } catch (IOException e) {
            throw new RuntimeException(e+"\n加载失败");
        }
    }
}
