package com.bluedot.dao;

import org.junit.Test;

import java.io.*;


public class Test01 {


    @Test
    public void Test01() throws IOException, InterruptedException {
        Process process = null;
        File dir = new File("D:\\");
        String EXE_PATH = "D:\\Untitled1.exe";
        Runtime runtime = Runtime.getRuntime();
        process = runtime.exec(EXE_PATH,null, dir);
        OutputStream outputStream = process.getOutputStream();
        PrintWriter outputWriter = new PrintWriter(outputStream, true);


        outputWriter.print(1);
        outputWriter.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = null;
        while ((line = in.readLine()) != null) {
            System.out.println(line);
        }
        in.close();
        process.waitFor();

    }

    @Test
    public void Test02() throws IOException, InterruptedException {
        // 本质输入命令
        Process process = null;

        String EXE_PATH = "D:\\Untitled1.exe";
        Runtime runtime = Runtime.getRuntime();
        process = runtime.exec(EXE_PATH);
        OutputStream outputStream = process.getOutputStream();
        PrintWriter outputWriter = new PrintWriter(outputStream, true);
        outputWriter.print(2);
        outputWriter.close();
        BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = null;
        while ((line = in.readLine()) != null) {
            System.out.println(line);
        }
        in.close();
        process.waitFor();

    }


}
