package com.bluedot.dao;

import com.alibaba.fastjson.JSON;
import com.bluedot.pojo.pack.PreMappedStatement;
import com.bluedot.pojo.vo.RequestVO;
import com.bluedot.pojo.vo.RoleVO;
import com.bluedot.pojo.vo.UserVO;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class SQLTest {
    Connection connection = null;
    @Before
    public void initData(){
        try {
            // 加载hsqldb驱动
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.connection = DriverManager
                    .getConnection("jdbc:mysql://rm-bp1mqos7k6lt0a653to.mysql.rds.aliyuncs.com:3306/electrochemicalanalysissystem?characterEncoding=utf8&useSSL=false&serverTimezone=UTC&rewriteBatchedStatements=true&allowPublicKeyRetrieval=true",
                            "root",
                            "Unlimited2002");
        } catch (Exception e) {
            e.printStackTrace();
        }

        UserVO userVO = new UserVO("1","admin","123123","男","15970916646","南昌","xxxx@qq.com",1,new ArrayList<RoleVO>(){
            {
                add(new RoleVO(12,"管理员","管理员负责管理用户",new ArrayList<String>(){
                    {
                        add("用户管理");
                    }
                }));
            }
        },new ArrayList<String>(){
            {
                add("/user/login");
            }
        });
    }

    /**
     * 测试SQL
     */
    @Test
    public void update() throws SQLException {
        UserVO userVO = new UserVO("3", "you","123123","男","15970916646","赣州","xxx1x@qq.com",1,
            null,null);
        PreMappedStatement<UserVO> query = new PreMappedStatement<>(
                "query", userVO

        );
        SQL sql = new SQL();
        Object update = sql.update(query);
        System.out.println(update);
    }

    @Test
    public void test() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {

    }
}